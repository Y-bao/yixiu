package com.Ybao.yixiu.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.Ybao.yixiu.R;

public class NameActivity extends Activity implements OnClickListener
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_name);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.title_bar);
		}
		TextView vctex = (TextView) findViewById(R.id.titletext);
		vctex.setText("����");
		Button vcbt = (Button) findViewById(R.id.l_bt);
		vcbt.setText("�༭�б�");
		vcbt.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0)
	{
		KeyEvent myKey = new KeyEvent(0, 4);
		this.onKeyDown(4, myKey);
	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			TextView vcname = (TextView) findViewById(R.id.my_uesrname);
			Intent intent = new Intent();
			intent.putExtra("eidt_name", vcname.getText().toString());
			setResult(1, intent);
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}
}
