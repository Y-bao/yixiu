package com.Ybao.yixiu.Way;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.Ybao.yixiu.Entity.ListItem;

public class MyAdapter
{
	public static class MyGuideAdapter extends PagerAdapter
	{
		private ArrayList<View> vpageList;

		public MyGuideAdapter(ArrayList<View> vpageList)
		{
			this.vpageList = vpageList;
		}

		@Override
		public int getCount()
		{
			if (vpageList != null) return vpageList.size();
			else return 0;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1)
		{
			if (vpageList != null)
			{
				((ViewPager) arg0).addView(vpageList.get(arg1));
				return vpageList.get(arg1);
			}
			else
			{
				return null;
			}
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2)
		{
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1)
		{
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1)
		{

		}

		@Override
		public Parcelable saveState()
		{
			return null;
		}

		@Override
		public void startUpdate(View arg0)
		{

		}

		@Override
		public void finishUpdate(View arg0)
		{

		}
	}

	public static class MyFragmentAdapter extends FragmentPagerAdapter
	{
		private ArrayList<Fragment> paferlist;

		public MyFragmentAdapter(FragmentManager fm, ArrayList<Fragment> paferlist)
		{
			super(fm);
			this.paferlist = paferlist;
		}

		@Override
		public Fragment getItem(int index)
		{
			if (index >= 0 && index < paferlist.size())
			{
				return paferlist.get(index);
			}
			else
			{
				return paferlist.get(0);
			}
		}

		@Override
		public int getCount()
		{
			if (paferlist != null)
			{
				return paferlist.size();
			}
			return 0;
		}
	}

	public static class mtleItemList extends BaseAdapter
	{
		private List<ListItem> mData;
		private LayoutInflater mInflater;
		private ArrayList<Integer> TypeList = new ArrayList<Integer>();

		public void AddType(int mResource)
		{
			TypeList.add(mResource);
		}

		public mtleItemList(Context context, List<ListItem> data)
		{
			mData = data;
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getItemViewType(int position)
		{
			return mData.get(position).mType;
		}

		@Override
		public int getViewTypeCount()
		{
			if (TypeList.size() == 0) return 1;
			else return TypeList.size();
		}

		public int getCount()
		{
			// TODO Auto-generated method stub
			return mData.size();
		}

		public ListItem getItem(int position)
		{
			// TODO Auto-generated method stub
			return mData.get(position);
			// return null;
		}

		public long getItemId(int position)
		{
			// TODO Auto-generated method stub
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent)
		{
			// TODO Auto-generated method stub
			ViewHolder holder = null;
			int type = getItemViewType(position);

			if (convertView == null)
			{
				holder = new ViewHolder();
				ListItem item = getItem(position);
				convertView = mInflater.inflate(TypeList.get(type), null);
				// convertView = mInflater.inflate(type,parent,false);
				for (Iterator<Integer> it = item.mMap.keySet().iterator(); it.hasNext();)
				{ // ����
					int id = it.next();
					Object obj = convertView.findViewById(id);
					if (obj != null)
					{
						holder.List_Object.add(obj);
						holder.List_id.add(id);
					}
				}
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			holder.SetValue(mData.get(position));
			return convertView;
		}

		public static class ViewHolder
		{
			ArrayList<Object> List_Object = new ArrayList<Object>();
			ArrayList<Integer> List_id = new ArrayList<Integer>();

			public boolean SetValue(ListItem item)
			{
				int i = 0;
				Object oV;
				for (Object obj : List_Object)
				{
					int id = List_id.get(i);
					oV = item.mMap.get(id);

					if (obj.getClass().equals(TextView.class))
					{
						((TextView) obj).setText(oV.toString());
					}

					if (obj.getClass().equals(ImageView.class))
					{
						if (oV.getClass().equals(Integer.class))
						{
							((ImageView) obj).setImageResource((Integer) oV);
						}
					}

					if (obj.getClass().equals(ImageButton.class))
					{
						if (oV.getClass().equals(Integer.class))
						{
							((ImageButton) obj).setImageResource((Integer) oV);
						}

						if (oV.getClass().equals(View.OnClickListener.class))
						{
							((ImageButton) obj).setOnClickListener((View.OnClickListener) oV);
						}
					}

					i++;
				}
				return false;
			}
		}
	}
}
