package com.Ybao.yixiu.View.WheelView;

public interface OnWheelChangedListener
{
	void onChanged(WheelView wheel, int oldValue, int newValue);
}
