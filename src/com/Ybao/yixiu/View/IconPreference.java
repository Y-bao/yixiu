package com.Ybao.yixiu.View;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Ybao.yixiu.R;

public class IconPreference extends Preference
{
	protected Drawable itemDrawable;

	ImageView imageView;

	public IconPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.NewPreferenceElement);
		itemDrawable = array.getDrawable(R.styleable.NewPreferenceElement_img_icon);
		array.recycle();
	}

	@Override
	protected void onBindView(View view)
	{
		super.onBindView(view);
		imageView = (ImageView) view.findViewById(R.id.img_icon);
		imageView.setImageDrawable(itemDrawable);
		TextView title = (TextView) view.findViewById(R.id.title_icon);
		title.setText(getTitle());
	}

	public void setIcon(int icon)
	{
		itemDrawable = getContext().getResources().getDrawable(icon);
	}

}
