package com.Ybao.yixiu.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Entity.RefreshList;
import com.Ybao.yixiu.Entity.RefreshList.onRefrash;

public class DraftActivity extends Activity implements OnClickListener,
		OnItemClickListener
{

	ListView mListView;
	public static RefreshList mRefreshList = new RefreshList();
	BaseAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_draft_box);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("�ݸ���");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("����");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
		mListView = (ListView) findViewById(R.id.draft_list);
		mListView.setEmptyView(findViewById(R.id.empty));
		mRefreshList.setOnRefrash(myRefrash);
		mAdapter = new SimpleAdapter(this, mRefreshList.List,
				R.layout.message_item, new String[] { "Name", "Time" },
				new int[] { R.id.title, R.id.info });
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
	}

	private onRefrash myRefrash = new onRefrash()
	{
		@Override
		public void onCanRefrash()
		{
			Message msg = myHandler.obtainMessage();
			myHandler.sendMessage(msg);
		}
	};

	Handler myHandler = new Handler()
	{

		@Override
		public void handleMessage(Message msg)
		{
			mAdapter.notifyDataSetChanged();
		}

	};

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		Intent intent = new Intent();
		intent.putExtra("index", arg2);
		intent.setClass(this, EidtActivity.class);
		startActivity(intent);
	}

	@Override
	public void onClick(View arg0)
	{
		String tag = arg0.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			this.onKeyDown(4, myKey);
		}
	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}

}
