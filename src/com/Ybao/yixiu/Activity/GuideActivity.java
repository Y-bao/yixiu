package com.Ybao.yixiu.Activity;

import java.util.ArrayList;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Way.MyAdapter;
import com.Ybao.yixiu.Way.ToBeThumbnail;

public class GuideActivity extends FragmentActivity
{
	private ArrayList<View> vpageList;
	private ArrayList<View> vpageListp;
	private View[] vpageid;
	private ViewPager guidePager;
	private int[] ViewBgId;
	private Animation mAnimation;
	private int nowguide;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.guide);
		init();
		addpager();
		addpagerid();
	}

	private void init()
	{
		vpageList = new ArrayList<View>();
		vpageListp = new ArrayList<View>();
		mAnimation = (Animation) AnimationUtils.loadAnimation(this,
				R.anim.show_anim);
		int[] mViewBgId = { R.drawable.update_info_01_0,
				R.drawable.update_info_01_1, R.drawable.update_info_02_0,
				R.drawable.update_info_02_1, R.drawable.update_info_03_0,
				R.drawable.update_info_03_1, R.drawable.update_info_04_0,
				R.drawable.update_info_04_1 };
		ViewBgId = mViewBgId;
	}

	private void addpagerid()
	{
		vpageid = new View[vpageList.size()];
		LinearLayout llLayout = (LinearLayout) findViewById(R.id.guide_id);
		if (llLayout != null && llLayout.getChildCount() < ViewBgId.length / 3)
		{
			for (int i = 0; i < ViewBgId.length - 1; i++)
			{
				View mView = new View(this);
				LayoutParams mParams = new LayoutParams(15, 15);
				mView.setLayoutParams(mParams);
				if (i % 2 == 0)
				{
					mView.setBackgroundResource(R.drawable.guide_item_id);
					mView.setEnabled(false);
					vpageid[i / 2] = mView;
				}
				llLayout.addView(mView);
			}
			nowguide = 0;
			vpageid[0].setEnabled(true);
		}
	}

	private void addpager()
	{
		for (int i = 0; i < ViewBgId.length / 2; i++)
		{
			RelativeLayout mLayout = new RelativeLayout(this);
			Drawable mdrawable1 = getResources().getDrawable(ViewBgId[i * 2]);
			mLayout.setBackgroundDrawable(ToBeThumbnail.zipDrawable(mdrawable1,
					320, 480));
			Drawable mdrawable2 = getResources().getDrawable(
					ViewBgId[i * 2 + 1]);
			View mView = new View(this);
			mView.setBackgroundDrawable(ToBeThumbnail.zipDrawable(mdrawable2,
					320, 480));
			mView.setVisibility(View.INVISIBLE);
			vpageListp.add(mView);
			mLayout.addView(mView);
			vpageList.add(mLayout);
		}
		vpageListp.get(0).setVisibility(View.VISIBLE);
		vpageListp.get(0).setAnimation(mAnimation);
		guidePager = (ViewPager) findViewById(R.id.guide);
		guidePager.setAdapter(new MyAdapter.MyGuideAdapter(vpageList));
		guidePager.setOnPageChangeListener(mPageChangeListener);

	}

	private OnPageChangeListener mPageChangeListener = new OnPageChangeListener()
	{

		@Override
		public void onPageSelected(int arg0)
		{
			if (arg0 > 0 && arg0 < vpageListp.size())
			{
				if (vpageListp.get(arg0).getVisibility() == View.INVISIBLE)
				{
					vpageListp.get(arg0).setVisibility(View.VISIBLE);
					vpageListp.get(arg0).startAnimation(mAnimation);
				}
				vpageid[arg0].setEnabled(true);
				vpageid[nowguide].setEnabled(false);
				nowguide = arg0;
				Button b2 = (Button) findViewById(R.id.bt2);
				if (arg0 == vpageListp.size() - 1
						&& b2.getVisibility() == View.INVISIBLE)
				{
					b2.setVisibility(View.VISIBLE);
					b2.setOnClickListener(mClickListener);
				}
				else
				{
					b2.setVisibility(View.INVISIBLE);
				}
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0)
		{
			// TODO Auto-generated method stub

		}
	};
	private OnClickListener mClickListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{

			SharedPreferences preferences = getSharedPreferences("first_pref",
					MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putBoolean("isFirstIn", false);
			editor.commit();
			Intent intent = new Intent();
			intent.setClass(GuideActivity.this, MainActivity.class);
			startActivity(intent);
			finish();
		}
	};
}
