package com.Ybao.yixiu.Entity;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

public class Message
{

	public Map<String, Object> Parameters;
	public Map<String, Object> Results;
	public String Type;

	public String toJson()
	{
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public Message(String type)
	{
		Parameters = new HashMap<String, Object>();
		Results = new HashMap<String, Object>();
		Type = type;
	}

	public static Message getMessage(String str)
	{
		Gson gson = new Gson();
		return gson.fromJson(str, Message.class);
	}
}
