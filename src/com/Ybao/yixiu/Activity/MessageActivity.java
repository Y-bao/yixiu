package com.Ybao.yixiu.Activity;

import java.util.Map;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Fragment.Page1Fragment;
import com.Ybao.yixiu.Fragment.Page2Fragment;

public class MessageActivity extends PreferenceActivity implements
		OnClickListener
{

	private String[] preferenceKey = { "key_message_name", "key_time_start",
			"key_time_end", "key_message_type", "key_message_txt" };
	private String[] tag = { "Name", "Time", "Time", "Type", "content" };

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.message_list);
		setContentView(R.layout.activity_apply_view);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("��ϸ����");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("����");
		tvbt.setOnClickListener(this);
		Bundle dBundle = getIntent().getExtras();
		int index = dBundle.getInt("index");
		if (index < Page2Fragment.mRefreshList.getSize())
		{
			Map<String, Object> map = Page1Fragment.mRefreshList.getItem(index);
			for (int i = 0; i < preferenceKey.length; i++)
			{
				if (i == 1)
				{
					Object object = map.get(tag[i]);
					if (object != null)
					{
						String[] time = object.toString().split("~");
						Preference p = findPreference(preferenceKey[i]);
						p.setSummary(time[0]);
						p = findPreference(preferenceKey[i + 1]);
						p.setSummary(time[1]);
					}
				}
				else if (i == 3)
				{
					Preference p = findPreference(preferenceKey[i]);
					p.setSummary("�ż�");
				}
				else if (i != 2)
				{
					Preference p = findPreference(preferenceKey[i]);
					Object object = map.get(tag[i]);
					if (object != null)
					{
						p.setSummary(object.toString());
					}
				}
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		KeyEvent myKey = new KeyEvent(0, 4);
		this.onKeyDown(4, myKey);
	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}
}
