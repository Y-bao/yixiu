package com.Ybao.yixiu.Json;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjectToJson
{
	public static String toJson(String key, Object obj)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put(key, obj);
		}
		catch (JSONException e)
		{
			return "";
		}

		return jsonObject.toString();
	}
}
