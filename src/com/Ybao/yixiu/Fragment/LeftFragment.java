package com.Ybao.yixiu.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Activity.AboutActivity;
import com.Ybao.yixiu.Activity.InfosActivity;
import com.Ybao.yixiu.Activity.LoginActivity;
import com.Ybao.yixiu.Activity.PaintActivity;
import com.Ybao.yixiu.Function.LoginFunction;
import com.Ybao.yixiu.Ini.UesrData;

public class LeftFragment extends Fragment implements OnClickListener
{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View mView = inflater.inflate(R.layout.left_fragment, container, false);
		ListView L1 = (ListView) mView.findViewById(R.id.cc);
		SimpleAdapter sa1 = new SimpleAdapter(inflater.getContext(), list, R.layout.ce_item, new String[] { "abc" },
				new int[] { R.id.info });
		L1.setAdapter(sa1);
		L1.setOnItemClickListener(lClick);
		ListView L2 = (ListView) mView.findViewById(R.id.gd);
		SimpleAdapter sa2 = new SimpleAdapter(inflater.getContext(), list2, R.layout.ce_item, new String[] { "abc" },
				new int[] { R.id.info });
		L2.setAdapter(sa2);
		L2.setOnItemClickListener(aboutClick);
		Button exit_bt = (Button) mView.findViewById(R.id.exit_bt);
		exit_bt.setTag("exit_bt");
		exit_bt.setOnClickListener(this);
		TextView main_name = (TextView) mView.findViewById(R.id.main_name);
		TextView main_sex = (TextView) mView.findViewById(R.id.main_sex);
		TextView main_pos = (TextView) mView.findViewById(R.id.main_pos);
		TextView main_phone = (TextView) mView.findViewById(R.id.main_phone);
		main_name.setText(UesrData.name);
		main_sex.setText(UesrData.sex);
		main_pos.setText(UesrData.position);
		main_phone.setText("手机:" + UesrData.phone);
		LinearLayout ce_hLayout = (LinearLayout) mView.findViewById(R.id.ce_h);
		ce_hLayout.setTag("ce_h");
		ce_hLayout.setOnClickListener(this);
		return mView;
	}

	public static ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> list2 = new ArrayList<HashMap<String, String>>();
	static
	{
		HashMap<String, String> m1 = new HashMap<String, String>();
		m1.put("abc", "通过率");
		list.add(m1);
		HashMap<String, String> m4 = new HashMap<String, String>();
		m4.put("abc", "账号注销");
		list.add(m4);
		HashMap<String, String> m5 = new HashMap<String, String>();
		m5.put("abc", "笔迹签名");
		list.add(m5);
		HashMap<String, String> m6 = new HashMap<String, String>();
		m6.put("abc", "关于");
		list2.add(m6);

	}
	OnItemClickListener aboutClick = new OnItemClickListener()
	{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), AboutActivity.class);
			startActivity(intent);
		}
	};
	OnItemClickListener lClick = new OnItemClickListener()
	{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{
			if (arg2 == 2)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), PaintActivity.class);
				startActivity(intent);
			}
			else if (arg2 == 1)
			{
				LoginFunction.Close();
				Intent intent = new Intent();
				intent.setClass(getActivity(), LoginActivity.class);
				startActivity(intent);
				getActivity().finish();

			}
		}
	};

	@Override
	public void onClick(View arg0)
	{
		if (arg0.getTag().equals("ce_h"))
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), InfosActivity.class);
			startActivity(intent);
		}
		else if (arg0.getTag().equals("exit_bt"))
		{
			LoginFunction.Close();
			getActivity().finish();
		}
	}
}
