package com.Ybao.yixiu.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;

public class SimpleSlideGroup extends RelativeLayout
{
	protected Context mContext;
	protected Scroller mScroller;
	protected float mLastMotionX = 0;
	protected VelocityTracker mVelocityTracker = null;
	protected int mTouchSlop;
	protected int mDuration = 350;
	protected float mVelocity = 0.5f;
	protected boolean mIsBeingDragged = false;
	protected static int SNAP_VELOCITY = 600;
	protected View leftView, rightView;
	protected View centerGroup;

	public SimpleSlideGroup(Context context)
	{
		super(context);
		mContext = context;
		init(context);
	}

	public SimpleSlideGroup(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;
		init(context);
	}

	private void init(Context context)
	{
		mScroller = new Scroller(context, new DecelerateInterpolator());
		mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
	}

	public void addLeftView(View view)
	{
		LayoutParams behindParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
		behindParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		addView(view, behindParams);
		leftView = view;
	}

	public void addRightView(View view)
	{
		LayoutParams behindParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
		behindParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		addView(view, behindParams);
		rightView = view;
	}

	public void addcenterGroup(View view)
	{

		addView(view);
		view.bringToFront();
		centerGroup = view;

	}

	@Override
	public void computeScroll()
	{
		if (mScroller.computeScrollOffset())
		{
			centerGroup.scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
			invalidate();
		}
	}

	// 这个感觉没什么作用 不管true还是false 都是会执行onTouchEvent的 因为子view里面onTouchEvent返回false了
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		final int action = ev.getAction();
		final float x = ev.getX();

		if (x < -centerGroup.getScrollX() || x > centerGroup.getWidth() - centerGroup.getScrollX())
		{
			return false;
		}
		switch (action)
		{
		case MotionEvent.ACTION_MOVE:
			final int xDiff = (int) (mLastMotionX - x);
			if (Math.abs(xDiff) > mTouchSlop)
			{
				mIsBeingDragged = true;
			}
			break;
		case MotionEvent.ACTION_DOWN:
			if (mScroller != null)
			{
				if (!mScroller.isFinished())
				{
					mIsBeingDragged = true;
					mScroller.abortAnimation();
				}
			}
			mLastMotionX = x;
			break;
		}
		return mIsBeingDragged;
	}

	public boolean onTouchEvent(MotionEvent event)
	{
		if (mVelocityTracker == null)
		{
			mVelocityTracker = VelocityTracker.obtain();
		}
		mVelocityTracker.addMovement(event);
		float x = event.getX();
		float oldScrollX = centerGroup.getScrollX();

		switch (event.getAction())
		{
		case MotionEvent.ACTION_MOVE:
			int detaX = (int) (mLastMotionX - x);
			if (mIsBeingDragged == true && -getLeftWidth() <= (oldScrollX + detaX)
					&& (oldScrollX + detaX) <= getRightWidth())
			{
				if (oldScrollX + detaX > 0)
				{
					showRight();
				}
				else
				{
					showLeft();
				}
				centerGroup.scrollBy(detaX, 0);
			}

			mLastMotionX = x;

			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			mVelocityTracker.computeCurrentVelocity(1000);
			if (mIsBeingDragged == true)
			{
				float mVelocityX = mVelocityTracker.getXVelocity();
				if ((mVelocityX > SNAP_VELOCITY && oldScrollX == 0) || oldScrollX < -getLeftWidth() / 2)
				{
					showLeft();
					smoothScrollToByVelocity((int) (-getLeftWidth() - oldScrollX));
				}
				else if ((-mVelocityX > SNAP_VELOCITY && oldScrollX == 0) || oldScrollX > getRightWidth() / 2)
				{
					showRight();
					smoothScrollToByVelocity((int) (getRightWidth() - oldScrollX));
				}
				else if (oldScrollX != 0)
				{
					smoothScrollToByVelocity((int) -oldScrollX);
				}
			}
			mIsBeingDragged = false;
			mVelocityTracker.recycle();
			break;
		}

		return true;
	}

	public int getLeftWidth()
	{
		if (leftView != null)
		{
			return leftView.getWidth();
		}
		return 0;
	}

	public int getRightWidth()
	{
		if (rightView != null)
		{
			return rightView.getWidth();
		}
		return 0;
	}

	public void showLeft()
	{
		if (leftView == null || rightView == null || rightView.getVisibility() == View.GONE)
		{
			return;
		}
		leftView.setVisibility(View.VISIBLE);
		rightView.setVisibility(View.GONE);
	}

	public void showRight()
	{
		if (leftView == null || rightView == null || leftView.getVisibility() == View.GONE)
		{
			return;
		}
		leftView.setVisibility(View.GONE);
		rightView.setVisibility(View.VISIBLE);
	}

	protected void smoothScrollToByVelocity(int dx)
	{
		int oldScrollX = centerGroup.getScrollX();
		int oldScrollY = centerGroup.getScrollY();
		mScroller.startScroll(oldScrollX, oldScrollY, dx, 0, (int) Math.abs(dx / mVelocity));
		invalidate();
	}

	protected void smoothScrollToByTime(int dx)
	{
		int oldScrollX = centerGroup.getScrollX();
		int oldScrollY = centerGroup.getScrollY();
		mScroller.startScroll(oldScrollX, oldScrollY, dx, 0, mDuration);
		invalidate();
	}

	public void setDuration(int duration)
	{
		mDuration = duration;
	}

	public void showLeftView()
	{
		int oldScrollX = centerGroup.getScrollX();
		if (oldScrollX == 0)
		{
			leftView.setVisibility(View.VISIBLE);
			rightView.setVisibility(View.INVISIBLE);
			smoothScrollToByVelocity(-getLeftWidth());
		}
		else if (oldScrollX == -getLeftWidth())
		{
			smoothScrollToByVelocity(getLeftWidth());
		}
	}

	public void showRightView()
	{
		int oldScrollX = centerGroup.getScrollX();
		if (oldScrollX == 0)
		{
			leftView.setVisibility(View.INVISIBLE);
			rightView.setVisibility(View.VISIBLE);
			smoothScrollToByVelocity(getRightWidth());
		}
		else if (oldScrollX == getRightWidth())
		{
			smoothScrollToByVelocity(-getRightWidth());
		}
	}

}
