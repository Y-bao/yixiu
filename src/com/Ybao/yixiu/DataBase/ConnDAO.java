package com.Ybao.yixiu.DataBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.Ybao.yixiu.R;

public class ConnDAO
{
	private SQLiteDatabase db;
	private DBOpenHelper openHelper;
	private static int[] imgid = { R.drawable.apply_agreed, R.drawable.apply_ing, R.drawable.apply_disagreed };

	public ConnDAO(Context context)
	{
		openHelper = new DBOpenHelper(context);
	}

	public synchronized void ExecuteProcedure(List<Map<String, Object>> list, String table)
	{
		db = openHelper.getReadableDatabase();
		if (table.equalsIgnoreCase("Applys"))
		{
			for (Map<String, Object> map : list)
			{
				db.execSQL(
						"insert into Applys (Uesrs_ID,Updatas_ID,Title,Text,Time,StateID,SetTime,Type) values (?,?,?,?,?,?,?,?);",
						new Object[] { map.get("Uesrs_ID"), map.get("Updatas_ID"), map.get("Title"), map.get("Text"),
								map.get("Time"), map.get("StateID"), map.get("SetTime"), map.get("Type") });
			}
		}
		else if (table.equalsIgnoreCase("Messages"))
		{
			for (Map<String, Object> map : list)
			{
				db.execSQL(
						"insert into Messages (Uesrs_ID,Updatas_ID,Title,Text,Time,SetTime,Type) values (?,?,?,?,?,?,?);",
						new Object[] { map.get("Uesrs_ID"), map.get("Updatas_ID"), map.get("Title"), map.get("Text"),
								map.get("Time"), map.get("SetTime"), map.get("Type") });
			}
		}
		db.close();
	}

	public synchronized void ExecuteProcedure(Map<String, Object> map, String table)
	{
		db = openHelper.getReadableDatabase();
		if (table.equalsIgnoreCase("t_apply"))
		{
			db.execSQL(
					"insert into t_apply (Title,Time,Type,content,State,StateID,setTime) values (?,?,?,?,?,?,?);",
					new Object[] { map.get("title"), map.get("time"), map.get("type"), map.get("context"),
							imgid[Integer.parseInt(map.get("stateID").toString())], map.get("stateID"),
							map.get("setTime") });
		}
		else if (table.equalsIgnoreCase("t_message"))
		{
			db.execSQL("insert into t_message (Title,Time,content,setTime) values (?,?,?,?);",
					new Object[] { map.get("title"), map.get("time"), map.get("context"), map.get("setTime") });
		}
		else if (table.equalsIgnoreCase("t_updata"))
		{
			db.execSQL("insert into t_updata (Time) values (?);", new Object[] { map.get("Time") });
		}
		db.close();
	}

	public synchronized List<Map<String, Object>> RuturnList(Map<String, Object> mapc, String table)
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		if (table.equalsIgnoreCase("t_apply"))
		{
			Cursor cursor = db.rawQuery("select Title,Time,Type,content,State,StateID,setTime from t_apply;", null);
			while (cursor.moveToNext())
			{
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("Name", cursor.getString(0));
				map.put("Time", cursor.getString(1));
				map.put("Type", cursor.getString(2));
				map.put("content", cursor.getString(3));
				map.put("State", cursor.getString(4));
				map.put("StateID", cursor.getString(5));
				map.put("setTime", cursor.getString(6));
				list.add(map);
			}
		}
		else if (table.equalsIgnoreCase("t_message"))
		{
			Cursor cursor = db.rawQuery("select mid,Title,Time,content,setTime from t_message;", null);
			while (cursor.moveToNext())
			{
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("Title", cursor.getString(1));
				map.put("Time", cursor.getString(2));
				map.put("content", cursor.getString(3));
				map.put("setTime", cursor.getString(4));
				list.add(map);
			}
		}
		return list;
	}

	public synchronized String RuturnNew(String table)
	{
		Cursor cursor = db.rawQuery("select Time from t_updata;", null);
		if (cursor.moveToNext())
		{
			return cursor.getString(0);
		}
		return null;
	}

}
