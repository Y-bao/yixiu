package com.Ybao.yixiu.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Activity.CaptureActivity;
import com.Ybao.yixiu.Activity.DraftActivity;
import com.Ybao.yixiu.Activity.EidtActivity;
import com.Ybao.yixiu.Activity.MainActivity;
import com.Ybao.yixiu.Activity.ModelActivity;
import com.Ybao.yixiu.Pool.ActivityPool;

public class RightFragment extends Fragment implements OnClickListener
{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View mView = inflater.inflate(R.layout.right_fragment, container, false);
		ListView l = (ListView) mView.findViewById(R.id.ce_list_r);
		SimpleAdapter s = new SimpleAdapter(inflater.getContext(), list, R.layout.ce_item_l, new String[] { "abc" },
				new int[] { R.id.tetle });
		l.setAdapter(s);
		l.setOnItemClickListener(listener);
		ImageButton CreateCode = (ImageButton) mView.findViewById(R.id.CreateCode);
		ImageButton QQbang = (ImageButton) mView.findViewById(R.id.QQbang);
		ImageButton Settings = (ImageButton) mView.findViewById(R.id.Settings);
		CreateCode.setTag("CreateCode");
		CreateCode.setOnClickListener(this);
		QQbang.setTag("QQbang");
		QQbang.setOnClickListener(this);
		Settings.setTag("Settings");
		Settings.setOnClickListener(this);
		return mView;
	}

	public static ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
	static
	{
		HashMap<String, String> m1 = new HashMap<String, String>();
		m1.put("abc", "д����");
		list.add(m1);
		HashMap<String, String> m2 = new HashMap<String, String>();
		m2.put("abc", "�ݸ���");
		list.add(m2);
		HashMap<String, String> m3 = new HashMap<String, String>();
		m3.put("abc", "ѡ��ģ��");
		list.add(m3);

	}

	OnItemClickListener listener = new OnItemClickListener()
	{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{
			if (arg3 == 0)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), EidtActivity.class);
				startActivity(intent);
			}
			else if (arg3 == 1)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), DraftActivity.class);
				startActivity(intent);
			}
			else if (arg3 == 2)
			{
				Intent intent = new Intent();
				intent.setClass(getActivity(), ModelActivity.class);
				startActivity(intent);
			}
		}

	};

	@Override
	public void onClick(View arg0)
	{
		String tag = arg0.getTag().toString();
		if (tag.equals("CreateCode"))
		{
			Intent intent = new Intent();
			intent.setClass(getActivity(), CaptureActivity.class);
			startActivity(intent);
		}
		else if (tag.equals("QQbang"))
		{
		}
		else if (tag.equals("Settings"))
		{
		}
	}
}
