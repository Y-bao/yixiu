package com.Ybao.yixiu.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.Ybao.yixiu.R;
import com.google.gson.Gson;

public class RefreshList
{
	public List<Map<String, Object>> List = new ArrayList<Map<String, Object>>();

	private static int[] imgid = { R.drawable.apply_agreed, R.drawable.apply_ing, R.drawable.apply_disagreed };

	public void addItem(Map<String, Object> item, int arg1)
	{
		if (arg1 == 0)
		{
			mRefrash.onCanRefrash();
		}
		else if (arg1 == 1)
		{
			List.add(item);
		}
	}

	@SuppressWarnings("unchecked")
	public void AddItemFromJson(String json, String table) throws InterruptedException
	{
		List<Map<String, Object>> mlist = new ArrayList<Map<String, Object>>();
		Gson gson = new Gson();
		mlist = gson.fromJson(json, mlist.getClass());
		if (table.equalsIgnoreCase("Applys"))
		{
			for (int i = 0; i < mlist.size(); i++)
			{
				mlist.get(i).put("State", imgid[(int) Double.parseDouble(mlist.get(i).get("StateID").toString())]);
				List.add(mlist.get(i));
			}
		}
		else if (table.equalsIgnoreCase("Messages"))
		{
			for (int i = 0; i < mlist.size(); i++)
			{
				List.add(mlist.get(i));
			}
		}
		while (true)
		{
			if (mRefrash != null)
			{
				mRefrash.onCanRefrash();
				break;
			}
			Thread.sleep(100);
		}
	}

	@SuppressWarnings("unchecked")
	public void PutItemFromJson(String json, String table) throws InterruptedException
	{
		Map<String, Object> map = new HashMap<String, Object>();
		Gson gson = new Gson();
		map = gson.fromJson(json, map.getClass());
		if (table.equalsIgnoreCase("Applys"))
		{
			map.put("State", imgid[(int) Double.parseDouble(map.get("StateID").toString())]);
			List.add(map);
		}
		else if (table.equalsIgnoreCase("Messages"))
		{
			List.add(map);
		}
		while (true)
		{
			if (mRefrash != null)
			{
				mRefrash.onCanRefrash();
				break;
			}
			Thread.sleep(100);
		}
	}

	public void setList(List<Map<String, Object>> List)
	{
		this.List = List;
		mRefrash.onCanRefrash();
	}

	public void removeItem(List<Map<String, Object>> List, int i)
	{
		List.remove(i);
		mRefrash.onCanRefrash();
	}

	public Map<String, Object> getItem(int index)
	{
		return List.get(index);
	}

	public int getSize()
	{
		return List.size();
	}

	public interface onRefrash
	{
		public void onCanRefrash();
	}

	private onRefrash mRefrash;

	public void setOnRefrash(onRefrash mRefrash)
	{
		this.mRefrash = mRefrash;
	}
}