package com.Ybao.yixiu.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper
{
	public static String name = "databases.db";
	public static int version = 1;

	public DBOpenHelper(Context context)
	{
		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL("create table Applys(ID integer primary key,Uesrs_ID integer,Updatas_ID integer,Title nvarchar(50),Text nvarchar(200),Time nvarchar(50),StateID integer,Type nvarchar(10),SetTime nvarchar(50),UpdataTime nvarchar(50));");
		db.execSQL("create table Messages(ID integer primary key,Uesrs_ID integer,Updatas_ID integer,Title nvarchar(50),Text nvarchar(200),Time nvarchar(50),SetTime nvarchar(50),Type nvarchar(10),UpdataTime nvarchar(50));");
		db.execSQL("create table Uesrs(ID integer primary key,Password nvarchar(20),StateID integer);");
		db.execSQL("create table UesrInfos(ID integer primary key,ParentID integer,ClassLayer integer,ClassList nvarchar(100),Nmae nvarchar(20),Old integer,Sex nvarchar(10),Phone nvarchar(20),ImgUrl nvarchar(100));");
		db.execSQL("create table Updatas(ID integer primary key,Uesrs_ID integer,UpDataTime nvarchar(50));");
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2)
	{

	}

}
