package com.Ybao.yixiu.Function;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Map;

import android.util.Log;

import com.Ybao.yixiu.Entity.Message;
import com.Ybao.yixiu.Ini.NetParamete;
import com.Ybao.yixiu.Ini.UesrData;
import com.google.gson.Gson;

public class DetailFunction
{
	public static InputStream in;
	public static OutputStream out;

	protected static boolean cion()
	{
		try
		{
			InetAddress address = InetAddress.getByName(NetParamete.IP);
			Socket socket = new Socket(address, NetParamete.Port1);
			socket.setSoTimeout(NetParamete.Timeout);
			out = socket.getOutputStream();
			in = socket.getInputStream();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public synchronized static void Macth()
	{
		try
		{
			Log.i("a", "st");
			if (!cion())
			{
				return;
			}
			Message msg = new Message("Macth");
			msg.Parameters.put("Id", UesrData.ID);
			Log.i("a", msg.toJson());
			out.write(msg.toJson().getBytes());
			out.flush();
			Log.i("a", "ok");
		}
		catch (Exception e)
		{
		}

	}

	public synchronized static void sendApply(Map<String, Object> map)
	{
		try
		{
			Message msg = new Message("Detail");
			msg.Parameters.put("Id", UesrData.ID);
			Gson gson = new Gson();
			msg.Results.put("msg", gson.toJson(map));
			Log.i("a",msg.toJson());
			out.write(msg.toJson().getBytes());
			out.flush();
			byte[] inReadB = new byte[1024];
			in.read(inReadB);
			Log.i("a","ok");
		}
		catch (Exception e)
		{
		}
	}
}
