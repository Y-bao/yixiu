package com.Ybao.yixiu.Entity;

import java.util.Map;

import com.Ybao.yixiu.Ini.MyEmun.Entity_Way;

public class Task
{
	private Entity_Way TaskID;
	private Map<String, Object> UesrMap;

	public Task(Entity_Way TaskID, Map<String, Object> UesrMap)
	{
		this.TaskID = TaskID;
		this.UesrMap = UesrMap;
	}

	public Entity_Way getTaskId()
	{
		return TaskID;
	}

	public Map<String, Object> getMap()
	{
		return UesrMap;
	}
}
