package com.Ybao.yixiu.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.Ybao.yixiu.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FriendListActivity extends Activity implements OnItemClickListener,OnClickListener
{

	BaseAdapter mAdapter;

	public static List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	static
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Title", "小明");
		map.put("Time", "好无聊");
		map.put("State", R.drawable.aio_face_default);
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("Title", "小红");
		map.put("Time", "好烦啊");
		map.put("State", R.drawable.aio_face_default);
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("Title", "小李");
		map.put("Time", "");
		map.put("State", R.drawable.aio_face_default);
		list.add(map);
	}

	public FriendListActivity()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_friends);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("好友列表");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("返回");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
		ListView mListView = (ListView) findViewById(R.id.friend_list);
		mAdapter = new SimpleAdapter(this, list, R.layout.apply_item, new String[] { "Title", "Time", "State" },
				new int[] { R.id.title, R.id.info, R.id.img });
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		Intent intent = new Intent();
		intent.setClass(this, DetailActivity.class);
		startActivity(intent);

	}

	@Override
	public void onClick(View arg0)
	{
		String tag = arg0.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			this.onKeyDown(4, myKey);
		}

	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}
}
