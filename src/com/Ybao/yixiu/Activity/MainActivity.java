package com.Ybao.yixiu.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Fragment.LeftFragment;
import com.Ybao.yixiu.Fragment.MainFragment;
import com.Ybao.yixiu.Fragment.MainFragment.MyPageChangeListener;
import com.Ybao.yixiu.Fragment.RightFragment;
import com.Ybao.yixiu.Pool.ActivityPool;
import com.Ybao.yixiu.View.ControllableSlideGroup;

public class MainActivity extends FragmentActivity
{
	private int[] fragmentlayout;
	private int[] fragmentid;
	public ControllableSlideGroup mLayout;
	public MainFragment mFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		init();
		layout_init();
		fragment_init();
		listener_init();
		ActivityPool.allActivity.add(this);
	}

	private void init()
	{
		int[] mfragmentlayout = { R.layout.left, R.layout.right, R.layout.center };
		int[] mfragmentid = { R.id.left, R.id.right, R.id.center };
		fragmentlayout = mfragmentlayout;
		fragmentid = mfragmentid;
	}

	private void listener_init()
	{
		mFragment.setMyPageChangeListener(mPageChangeListener);
	}

	private void layout_init()
	{
		mLayout = (ControllableSlideGroup) findViewById(R.id.main);
		if (mLayout != null)
		{
			mLayout.addLeftView(getLayoutInflater().inflate(fragmentlayout[0], null));
			mLayout.addRightView(getLayoutInflater().inflate(fragmentlayout[1], null));
			mLayout.addcenterGroup(getLayoutInflater().inflate(fragmentlayout[2], null));
		}
	}

	private void fragment_init()
	{
		for (int i = 0; i < fragmentlayout.length; i++)
		{
			if (findViewById(fragmentid[i]) == null)
			{
				return;
			}
		}
		FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
		LeftFragment lFragment = new LeftFragment();
		RightFragment rFragment = new RightFragment();
		mFragment = new MainFragment();
		mTransaction.replace(R.id.left, lFragment);
		mTransaction.replace(R.id.right, rFragment);
		mTransaction.replace(R.id.center, mFragment);
		mTransaction.commit();
	}

	private MyPageChangeListener mPageChangeListener = new MyPageChangeListener()
	{

		@Override
		public void onPageSelected(int position)
		{
			if (mFragment.isFirst())
			{
				mLayout.setCanSliding(true, false);
			}
			else if (mFragment.isEnd())
			{
				mLayout.setCanSliding(false, true);
			}
			else
			{
				mLayout.setCanSliding(false, false);
			}
			mFragment.topageid(position);
		}

		@Override
		public void onPageScrollStateChanged(int arg0)
		{

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2)
		{

		}

		@Override
		public void showDetailR()
		{
			// mLayout.showDetailViewR();

		}

		@Override
		public void showLeft()
		{
			mLayout.showLeftView();

		}

		@Override
		public void showRight()
		{
			mLayout.showRightView();

		}

		@Override
		public void showDetailL()
		{
			// mLayout.showDetailViewL();

		}
	};

	int count = 0;

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		if (KeyEvent.KEYCODE_BACK == keyCode)
		{
			// LoginFunction.Close();
			// NetSendThread.keepon = false;
			return false;

		}
		return false;
	}

}
