package com.Ybao.yixiu.Pool;

import java.util.ArrayList;

import android.app.Activity;

public class ActivityPool
{
	public static ArrayList<Activity> allActivity = new ArrayList<Activity>();

	public static Activity getActivityByName(String name)
	{

		for (Activity ac : allActivity)
		{
			if (ac.getClass().getName().indexOf(name) >= 0)
			{
				return ac;
			}
		}

		return null;
	}

}
