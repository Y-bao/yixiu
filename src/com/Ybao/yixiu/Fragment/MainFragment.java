package com.Ybao.yixiu.Fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Activity.FriendListActivity;
import com.Ybao.yixiu.Dialog.CustomDialog;
import com.Ybao.yixiu.Ini.MyEmun.MY_BT;
import com.Ybao.yixiu.Ini.UesrData;
import com.Ybao.yixiu.Way.MyAdapter;

public class MainFragment extends Fragment implements OnClickListener,OnTouchListener,AnimationListener
{

	private ArrayList<Fragment> pageList;
	private ViewPager mPager;
	private Button[] imgViews;
	private ImageView imgTag;
	private int nowpage;
	private RelativeLayout imgTagk;
	private RelativeLayout path_bt;
	private int wh;
	public Page1Fragment pFragment1;
	Toast q1;
	Toast q2;
	Toast q3;
	Toast q4;
	Toast q5;
	private boolean isclick=false;
	View mView;
	private int[] PageID = { R.layout.topagebt_boss, R.layout.topagebt_ex };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mView = inflater.inflate(R.layout.center_fragment, container, false);
		initLayout();
		mPager = (ViewPager) mView.findViewById(R.id.pager);
		mPager.setAdapter(new MyAdapter.MyFragmentAdapter(getChildFragmentManager(), pageList));
		mPager.setOnPageChangeListener(myPageChangeListener);

		RelativeLayout kk = (RelativeLayout) mView.findViewById(R.id.pagerbt_kk);
		try
		{
			getLayoutInflater(savedInstanceState).inflate(PageID[Integer.parseInt(UesrData.position)], kk, true);
		}
		catch (Exception e)
		{
			getLayoutInflater(savedInstanceState).inflate(PageID[1], kk, true);
		}
		init(mView);
		dataBind(mView);
		ImageButton friButton = (ImageButton) mView.findViewById(R.id.friend_bt);
		ImageButton showpathbt = (ImageButton) mView.findViewById(R.id.show_path_bt);
		path_bt=(RelativeLayout)mView.findViewById(R.id.path_bt);
		friButton.setTag("friend_bt");
		showpathbt.setTag("show_path_bt");
		friButton.setOnClickListener(this);
		showpathbt.setOnClickListener(this);
		mView.setTag("center_fragment");
		mView.setOnTouchListener(this);
		path_bt.setOnTouchListener(this);
		path_bt.setTag("path_bt_pass");
		return mView;
	}

	private void dataBind(View Fv)
	{
		//
	}

	private void initLayout()
	{
		pageList = new ArrayList<Fragment>();
		pFragment1 = new Page1Fragment();
		Page2Fragment pFragment2 = new Page2Fragment();
		pageList.add(pFragment1);
		pageList.add(pFragment2);
	}

	private void init(View Fv)
	{
		ImageView showLbt = (ImageView) Fv.findViewById(R.id.showL);
		ImageView showRbt = (ImageView) Fv.findViewById(R.id.showR);
		if (showLbt != null && showRbt != null)
		{
			showLbt.setTag(MY_BT.MAIN_SHOW_L_BT);
			showRbt.setTag(MY_BT.MAIN_SHOW_R_BT);
			showLbt.setOnClickListener(mClickListener);
			showRbt.setOnClickListener(mClickListener);
		}
		// ///////////////////////////////////////////////
		LinearLayout l1 = (LinearLayout) Fv.findViewById(R.id.pagerbt);
		imgViews = new Button[pageList.size()];
		imgTag = (ImageView) Fv.findViewById(R.id.idTag);
		imgTagk = (RelativeLayout) Fv.findViewById(R.id.idk);
		for (int i = 0; i < pageList.size(); i++)
		{
			imgViews[i] = (Button) l1.getChildAt(i);
			imgViews[i].setEnabled(true);
			imgViews[i].setTextColor(Color.GRAY);
			imgViews[i].setTag(i);
			imgViews[i].setOnClickListener(mClickListeners);
		}
		nowpage = 0;
		imgViews[nowpage].setEnabled(false);
		imgViews[nowpage].setTextColor(Color.WHITE);
	}

	public void topage(int index)
	{
		if (index >= 0 && index < pageList.size() && nowpage != index)
		{
			topageid(index);
			mPager.setCurrentItem(index);
		}
	}
	public void topageid(int index)
	{
		ArrayList<TranslateAnimation> animations = new ArrayList<TranslateAnimation>();
		wh = imgTagk.getWidth() - imgTag.getWidth();
		animations.add(new TranslateAnimation(wh, 0, 0, 0));
		animations.add(new TranslateAnimation(0, wh, 0, 0));
		if (index >= 0 && index < pageList.size())
		{
			animations.get(index).setFillAfter(true);
			animations.get(index).setDuration(500);
			imgTag.startAnimation(animations.get(index));
			imgViews[nowpage].setEnabled(true);
			imgViews[nowpage].setTextColor(Color.GRAY);
			imgViews[index].setEnabled(false);
			imgViews[index].setTextColor(Color.WHITE);
			nowpage = index;
		}
	}

	public boolean isFirst()
	{
		if (mPager.getCurrentItem() == 0) return true;
		else return false;
	}

	public boolean isEnd()
	{
		if (mPager.getCurrentItem() == pageList.size() - 1) return true;
		else return false;
	}

	private MyPageChangeListener myPageChangeListener;

	public void setMyPageChangeListener(MyPageChangeListener mPageChangeListener)
	{

		myPageChangeListener = mPageChangeListener;

	}

	public interface MyPageChangeListener extends OnPageChangeListener
	{
		public void onPageSelected(int position);

		public void showLeft();

		public void showRight();
		public void showDetailL();
		public void showDetailR();
	}

	private OnClickListener mClickListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			switch ((MY_BT) v.getTag())
			{
			case MAIN_SHOW_L_BT:
				myPageChangeListener.showLeft();
				break;
			case MAIN_SHOW_R_BT:
				myPageChangeListener.showRight();
				break;

			default:
				break;
			}

		}
	};
	private OnClickListener mClickListeners = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			int ponson = (Integer) v.getTag();
			topage(ponson);
		}
	};

	@Override
	public void onClick(View v)
	{
		if(v.getTag().equals("show_path_bt"))
		{
			if(isclick)
			{
				isclick=false;
				 RotateAnimation raAnimation=new RotateAnimation(0, -180,Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 1.0f);
				 raAnimation.setDuration(500);
				 raAnimation.setFillAfter(true);
					raAnimation.setAnimationListener(this);
					 path_bt.setVisibility(1);
				 path_bt.startAnimation(raAnimation);
			}
			else {
				isclick=true;
			 RotateAnimation raAnimation=new RotateAnimation(-180, 0,Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 1.0f);
			 raAnimation.setDuration(500);
			 raAnimation.setFillAfter(true);
			 path_bt.startAnimation(raAnimation);
			 path_bt.setVisibility(0);
			}
		}
		else if (v.getTag().equals("friend_bt")) 
		{
		Intent intent = new Intent();
		intent.setClass(getActivity(), FriendListActivity.class);
		startActivity(intent);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		
		if (v.getTag().equals("path_bt_pass")) 
		{
			if(!isclick)
			{
				return false;
			}
			float Y=v.getHeight()-event.getY();
			float X=v.getWidth()/2-event.getX();
			if(Math.sqrt(Y*Y+X*X)>280)
			{
				return false;
			}
			double red=0;
			if(X>0)
			{
				red=Math.toDegrees(Math.atan2(Y, X));
			}
			else {
				red=180-Math.toDegrees(Math.atan2(Y, -X));
			}
				CustomDialog dialog1 ;
				dialog1= new CustomDialog(this.getActivity(),280,350, R.layout.main_do, R.style.Theme_dialog);
				TextView dotitle=(TextView)dialog1.findViewById(R.id.dotitle);
				ImageView doicon=(ImageView)dialog1.findViewById(R.id.doicon);
			if(red>=9.5&&red<=41.7)
			{ 
				v.setBackgroundResource(R.drawable.bt_path_pass1);
				dotitle.setText("过期");
				doicon.setImageResource(R.drawable.doicon1);
			}
			else if(red>41.7&&red<=73.9)
			{
				v.setBackgroundResource(R.drawable.bt_path_pass2);
				dotitle.setText("分组");
				doicon.setImageResource(R.drawable.doicon2);
			}
			else if(red>73.9&&red<=106.1)
			{
				v.setBackgroundResource(R.drawable.bt_path_pass3);
				dotitle.setText("设置");
				doicon.setImageResource(R.drawable.doicon3);
			}
			else if(red>106.1&&red<=138.3)
			{
				v.setBackgroundResource(R.drawable.bt_path_pass4);
				dotitle.setText("排序");
				doicon.setImageResource(R.drawable.doicon4);
			}
			else if(red>138.3&&red<=170.5)
			{
				v.setBackgroundResource(R.drawable.bt_path_pass5);
				dotitle.setText("更多");
				doicon.setImageResource(R.drawable.doicon5);
			}
			dialog1.show();
			isclick=false;
			RotateAnimation raAnimation=new RotateAnimation(0, -180,Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 1.0f);
			raAnimation.setDuration(500);
			raAnimation.setFillAfter(true);
			raAnimation.setAnimationListener(this);
			v.setVisibility(0);
			v.startAnimation(raAnimation);
			return true;
		}
		else if (v.getTag().equals("center_fragment")) {

			return true;
		}
		return false;
	}

	@Override
	public void onAnimationEnd(Animation animation)
	{
		path_bt.setBackgroundResource(R.drawable.bt_path);
		
	}

	@Override
	public void onAnimationRepeat(Animation animation)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animation animation)
	{
		// TODO Auto-generated method stub
		
	}
}
