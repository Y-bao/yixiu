package com.Ybao.yixiu.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UpDataDAO
{
	private DBOpenHelper helper;
	private SQLiteDatabase db;

	public UpDataDAO(Context context)
	{
		helper = new DBOpenHelper(context);
		db = helper.getReadableDatabase();
	}

	public void add(String ss)
	{
		db.execSQL("insert into t_updata (Time) values (?);",
				new Object[] { ss });
	}

	public String getNewUpData()
	{
		Cursor cursor = db
				.rawQuery(
						"select Time from t_updata where id=(select max(id) from t_updata);",
						null);
		if (cursor.moveToNext())
		{
			return cursor.getString(0);
		}
		return null;
	}

	public void close()
	{
		db.close();
	}
}
