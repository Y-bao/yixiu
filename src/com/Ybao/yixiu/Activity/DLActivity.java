package com.Ybao.yixiu.Activity;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Ini.NetParamete;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DLActivity extends Activity implements OnClickListener
{

	public DLActivity()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generate
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_ip);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}
		TextView vctex = (TextView) findViewById(R.id.titletext);
		vctex.setText("����IP");
		TextView d_ip = (TextView) findViewById(R.id.d_ip);
		d_ip.setText(NetParamete.IP);
		Button vcbt = (Button) findViewById(R.id.l_bt);
		vcbt.setText("ȷ��");
		TextView IP1 = (TextView) findViewById(R.id.IP1);
		TextView IP2 = (TextView) findViewById(R.id.IP2);
		TextView IP3 = (TextView) findViewById(R.id.IP3);
		IP1.setText(NetParamete.IP1);
		IP2.setText(NetParamete.IP2);
		IP3.setText(NetParamete.IP3);
		IP1.setOnClickListener(this);
		IP2.setOnClickListener(this);
		IP3.setOnClickListener(this);
		vcbt.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0)
	{
		if (arg0.getId() == R.id.IP1)
		{
			NetParamete.IP = NetParamete.IP1;
			this.finish();
			return;
		}
		else if (arg0.getId() == R.id.IP2)
		{
			NetParamete.IP = NetParamete.IP2;
			this.finish();
			return;
		}
		else if (arg0.getId() == R.id.IP3)
		{
			NetParamete.IP = NetParamete.IP3;
			this.finish();
			return;
		}
		KeyEvent myKey = new KeyEvent(0, 4);
		this.onKeyDown(4, myKey);
	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			TextView vcname = (TextView) findViewById(R.id.my_uesrname);
			NetParamete.IP = vcname.getText().toString();
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}
}
