package com.Ybao.yixiu.Activity;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Fragment.MainFragment.MyPageChangeListener;
import com.Ybao.yixiu.View.WheelView.WheelMain;
import com.Ybao.yixiu.Way.MyAdapter.MyGuideAdapter;

public class TimeActivity extends Activity
{
	private ViewPager mPager;
	private ArrayList<View> pageList;
	private Button[] imgViews;
	private int nowpage;
	private ImageView imgTag;
	private RelativeLayout imgTagk;
	private int wh;
	private WheelMain wheelMain;
	private View timePicker1;
	private static int[] startTime = null;
	private static int[] endTime = null;
	int type;
	private Toast toast;

	public static int[] getStartTime()
	{
		return startTime;
	}

	public static int[] getEndTime()
	{
		return endTime;
	}

	public static void setStartTime(int[] Time)
	{
		startTime = Time;
	}

	public static void setEndTime(int[] Time)
	{
		endTime = Time;
	}

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_set);
		Bundle bundle = getIntent().getExtras();
		type = Integer.parseInt(bundle.get("time_type").toString());
		pageList = new ArrayList<View>();
		mPager = (ViewPager) findViewById(R.id.pcPager);
		View mView = new View(this);
		mView.setBackgroundResource(R.drawable.a1);
		View mView2 = new View(this);
		mView2.setBackgroundResource(R.drawable.a2);
		pageList.add(mView);
		pageList.add(mView2);
		mPager.setAdapter(new MyGuideAdapter(pageList));
		timePicker1 = findViewById(R.id.timePicker1);
		WheelMain.setSTART_YEAR(2004);
		WheelMain.setEND_YEAR(2040);
		wheelMain = new WheelMain(timePicker1);
		wheelMain.initDateTimePicker();
		Button tvbt = (Button) findViewById(R.id.time_l_bt);
		tvbt.setText("ȡ��");
		tvbt.setOnClickListener(mClickListener);

		init();

		mPager.setOnPageChangeListener(myPageChangeListener);
		toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
	}

	private void topage(int index)
	{
		if (index >= 0 && index < pageList.size() && nowpage != index)
		{
			mPager.setCurrentItem(index);
			topageid(index);
			if (index == 0)
			{
				endTime = wheelMain.getTime();
				if (startTime != null)
				{
					wheelMain.setTime(startTime[0], startTime[1], startTime[2]);
				}
			}
			else if (index == 1)
			{
				startTime = wheelMain.getTime();
				if (endTime != null)
				{
					wheelMain.setTime(endTime[0], endTime[1], endTime[2]);
				}
			}
		}
	}

	private MyPageChangeListener myPageChangeListener = new MyPageChangeListener()
	{

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2)
		{
		}

		@Override
		public void onPageScrollStateChanged(int arg0)
		{
		}

		@Override
		public void showRight()
		{
		}

		@Override
		public void showLeft()
		{
		}

		@Override
		public void showDetailL()
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void showDetailR()
		{
			// TODO Auto-generated method stub
			
		}


		@Override
		public void onPageSelected(int position)
		{
			topageid(position);
		}
	};

	public void topageid(int index)
	{
		wh = imgTagk.getWidth() - imgTag.getWidth();
		ArrayList<TranslateAnimation> animations = new ArrayList<TranslateAnimation>();
		animations.add(new TranslateAnimation(wh, 0, 0, 0));
		animations.add(new TranslateAnimation(0, wh, 0, 0));
		if (index >= 0 && index < pageList.size())
		{
			animations.get(index).setFillAfter(true);
			animations.get(index).setDuration(300);
			imgTag.startAnimation(animations.get(index));
			imgViews[nowpage].setEnabled(true);
			imgViews[nowpage].setTextColor(Color.GRAY);
			imgViews[index].setEnabled(false);
			imgViews[index].setTextColor(Color.WHITE);
			nowpage = index;
		}
	}

	private void init()
	{
		LinearLayout l1 = (LinearLayout) findViewById(R.id.time_page_bt);
		imgViews = new Button[pageList.size()];
		imgTag = (ImageView) findViewById(R.id.title_idTag);
		imgTagk = (RelativeLayout) findViewById(R.id.title_idk);

		for (int i = 0; i < pageList.size(); i++)
		{
			imgViews[i] = (Button) l1.getChildAt(i);
			imgViews[i].setEnabled(true);
			imgViews[i].setTextColor(Color.GRAY);
			imgViews[i].setTag(i);
			imgViews[i].setOnClickListener(mClickListeners);
		}
		if (type == 1)
		{
			nowpage = 0;
			if (startTime == null)
			{
				startTime = wheelMain.getTime();
			}
			else
			{
				wheelMain.setTime(startTime[0], startTime[1], startTime[2]);
			}
		}
		else if (type == 2)
		{
			nowpage = 1;
			mPager.setCurrentItem(1);
			if (endTime == null)
			{
				endTime = wheelMain.getTime();
			}
			else
			{
				wheelMain.setTime(endTime[0], endTime[1], endTime[2]);
			}
		}
		imgViews[nowpage].setEnabled(false);
		imgViews[nowpage].setTextColor(Color.WHITE);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	private OnClickListener mClickListeners = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			int ponson = (Integer) v.getTag();
			topage(ponson);
		}
	};

	private OnClickListener mClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View arg0)
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			TimeActivity.this.onKeyDown(4, myKey);
		}

	};

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			if (nowpage == 0)
			{
				startTime = wheelMain.getTime();
			}
			else if (nowpage == 1)
			{
				endTime = wheelMain.getTime();
			}
			if (startTime != null && endTime != null)
			{
				for (int i = 0; i < startTime.length; i++)
				{
					if (startTime[i] > endTime[i])
					{
						toast.show();
						startTime = null;
						endTime = null;
						return false;
					}
				}
			}
			setResult(2);
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}
}
