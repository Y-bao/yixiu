package com.Ybao.yixiu.Activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Ini.UesrData;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class IDCodeActivity extends Activity implements OnClickListener
{

	ImageView id_code;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_idcode);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("个人信息");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("返回");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
		TextView code_id = (TextView) findViewById(R.id.code_id);
		code_id.setText(Integer.toString(UesrData.ID));
		Bitmap bmp = null;
		id_code = (ImageView) findViewById(R.id.ID_Code);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Id", UesrData.ID);
		map.put("Name", UesrData.name);
		map.put("Sex", UesrData.sex);
		map.put("Old", UesrData.age);
		map.put("Phone", UesrData.phone);
		map.put("position", UesrData.position);
		Gson gson = new Gson();
		String json = gson.toJson(map);
		try
		{
			if (json != null && !"".equals(json))
			{
				bmp = CreateTwoDCode(json);
			}
		}
		catch (WriterException e)
		{
			e.printStackTrace();
		}
		if (bmp != null)
		{
			sendFingerBitmap(bmp);
			id_code.setImageBitmap(bmp);
		}
	}

	public Bitmap CreateTwoDCode(String content) throws WriterException
	{
		// 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
		BitMatrix matrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, 300, 300);
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		// 二维矩阵转为一维像素数组,也就是一直横着排了
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				if (matrix.get(x, y))
				{
					pixels[y * width + x] = 0xff000000;
				}
				else
				{
					pixels[y * width + x] = 0xffffffff;
				}
			}
		}

		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		// 通过像素数组生成bitmap,具体参考api
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}

	@SuppressLint("ShowToast")
	private void sendFingerBitmap(Bitmap bitmap)
	{
		// 读取SD卡状态
		boolean sdCardIsMounted = android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
		if (!sdCardIsMounted)
		{
			Toast.makeText(this, "请插入存储卡！", 1000).show();
		}
		else
		{
			File sdcard_path = Environment.getExternalStorageDirectory();
			String myFloder = getResources().getString(R.string.app_name);
			String timeStamp = Integer.toString(UesrData.ID);
			String suffixName = ".png";
			String filepath = sdcard_path + "/" + myFloder + "/" + timeStamp + suffixName;
			try
			{
				FileOutputStream fos = new FileOutputStream(new File(filepath));
				bitmap.compress(CompressFormat.PNG, 100, fos);
				fos.flush();
				fos.close();
				Toast.makeText(this, "保存成功！", 1000).show();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View arg0)
	{
		String tag = arg0.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			this.onKeyDown(4, myKey);
		}

	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}
}
