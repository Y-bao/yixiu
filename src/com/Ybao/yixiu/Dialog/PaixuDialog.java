package com.Ybao.yixiu.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Ybao.yixiu.R;

public class PaixuDialog extends Dialog
{

    private static int default_width = 220; //默认宽度
    private static int default_height = 400;//默认高度
    public static final int CICON=R.drawable.xicon;
    public static final int WICON=R.drawable.wicon;
    public static final int GICON=R.drawable.gicon;
    public int tiaojian=-1;
	public PaixuDialog(Context context,int icon,String title,String cent)
	{
		 this(context, default_width, default_height,icon, title, cent); 
	}
	 public PaixuDialog(Context context, int width, int height,int icon,String title,String cent) {
         super(context, R.style.Theme_dialog);
         //set content
        setContentView(R.layout.dialog_bt);
        ImageView dobt_icon=(ImageView)findViewById(R.id.dobticon);
        dobt_icon.setImageResource(icon);
        TextView dobt_title=(TextView)findViewById(R.id.dobttitle);
        dobt_title.setText(title);
        TextView dobt_cent=(TextView)findViewById(R.id.dobtcent);
        dobt_cent.setText(cent);
        
         //set window params
         Window window = getWindow();
         WindowManager.LayoutParams params = window.getAttributes();
         //set width,height by density and gravity
         float density = getDensity(context);
         params.width = (int) (width*density);
         params.height = (int) (height*density);
         params.gravity = Gravity.CENTER;
         window.setAttributes(params);
     }
     private float getDensity(Context context) {
         Resources resources = context.getResources();
         DisplayMetrics dm = resources.getDisplayMetrics();
        return dm.density;
     }
 	public void SetdoyesButton(android.view.View.OnClickListener onClickListener)
 	{
         Button doyesButton=(Button)findViewById(R.id.doyes);
         doyesButton.setOnClickListener(onClickListener);
 	}
	public void SetdonoButton(android.view.View.OnClickListener onClickListener)
	{
        Button donoButton=(Button)findViewById(R.id.dono);
        donoButton.setOnClickListener(onClickListener);
	}

}
