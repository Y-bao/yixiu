package com.Ybao.yixiu.Activity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Dialog.CustomDialog;
import com.Ybao.yixiu.Entity.Task;
import com.Ybao.yixiu.Function.LoginFunction;
import com.Ybao.yixiu.Ini.MyEmun.Entity_Way;
import com.Ybao.yixiu.Ini.MyEmun.MY_BT;
import com.Ybao.yixiu.Pool.ActivityPool;
import com.Ybao.yixiu.Thread.NetSendThread;
import com.Ybao.yixiu.Thread.NetService;
import com.Ybao.yixiu.Way.Rotate3d;

public class LoginActivity extends Activity
{
	private RelativeLayout mLayout;
	private LinearLayout loginPanel;
	private LinearLayout logingbar;
	private ImageView logoView;
	private Button inLogButton;
	private Button bqInfoButton;
	private Button loginButton;
	private EditText uesrnameEdit;
	private EditText passwardEdit;

	private Animation logoImgAnimation;
	private Animation bqBtAnimation;
	private Animation inLogBtAnimation;
	private Animation headImgAnimation;
	private Animation loginBtAnimation;
	private Animation loginPaAnimation;
	private Animation logingAnimation;
	private AnimationSet animationSet;
	private Rotate3d mRotate3d;
	private Toast SToast;
	private Toast DToast;
	private Toast CToast;
	private Toast KToast;
	private Toast AToast;
	private Toast WHToast;
	private Toast WToast;
	private Thread aThread;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		init_ViewGroup();
		init();
		init_Animations();
		start_Animations();
		set_Button_Click();
		ActivityPool.allActivity.add(this);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent();
		intent.setClass(LoginActivity.this, NetService.class);
		startService(intent);
	}

	@SuppressLint("ShowToast")
	public void init()
	{
		SToast = Toast.makeText(LoginActivity.this, "登陆成功！！", Toast.LENGTH_SHORT);
		DToast = Toast.makeText(LoginActivity.this, "帐号或密码错误！！", Toast.LENGTH_SHORT);
		CToast = Toast.makeText(LoginActivity.this, "服务器连接失败！！", Toast.LENGTH_LONG);
		KToast = Toast.makeText(LoginActivity.this, "不允许为空！！", Toast.LENGTH_SHORT);
		AToast = Toast.makeText(LoginActivity.this, "帐号不得少于4个字符！！", Toast.LENGTH_SHORT);
		WToast = Toast.makeText(LoginActivity.this, "该帐号含有不合法字符！！", Toast.LENGTH_SHORT);
		WHToast = Toast.makeText(LoginActivity.this, "未知错误！！", Toast.LENGTH_SHORT);
	}

	public void init_ViewGroup()
	{
		mLayout = (RelativeLayout) findViewById(R.id.welcome);
		logoView = (ImageView) findViewById(R.id.logo);
		bqInfoButton = (Button) findViewById(R.id.bq_info);
		inLogButton = (Button) findViewById(R.id.play_btn);
	}

	private void set_Button_Click()
	{
		inLogButton.setTag(MY_BT.WELCOME_INLOG_BT);
		inLogButton.setOnClickListener(mClickListener);
		bqInfoButton.setTag(MY_BT.WELCOME_BQ_BT);
		bqInfoButton.setOnClickListener(mClickListener);
	}

	private void init_Animations()
	{
		logoImgAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.logo_fade_in);
		bqBtAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.bq_fade_in);
		inLogBtAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.re_big_to_sm);
	}

	private void start_Animations()
	{
		logoView.startAnimation(logoImgAnimation);
		inLogButton.startAnimation(inLogBtAnimation);
		bqInfoButton.startAnimation(bqBtAnimation);
	}

	private OnClickListener mClickListener = new OnClickListener()
	{
		@SuppressLint("ShowToast")
		@Override
		public void onClick(View v)
		{
			switch ((MY_BT) v.getTag())
			{
			case WELCOME_INLOG_BT:
				int l[] = new int[2];
				logoView.getLocationOnScreen(l);
				TranslateAnimation mTranslateAnimation = new TranslateAnimation(0.0f, -l[0] * 1.0f
						+ logoView.getHeight() / 6, 0.0f, -l[1] * 1.0f + logoView.getHeight() / 2);
				ScaleAnimation mScaleAnimation = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f);
				animationSet = new AnimationSet(true);
				animationSet.addAnimation(mScaleAnimation);
				animationSet.addAnimation(mTranslateAnimation);
				animationSet.setFillAfter(true);
				animationSet.setDuration(600);
				animationSet.setAnimationListener(mAnimationListener);
				logoView.startAnimation(animationSet);
				Animation fadeOutAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.invi);
				inLogButton.startAnimation(fadeOutAnimation);
				bqInfoButton.startAnimation(fadeOutAnimation);
				break;
			case WELCOME_LOGIN_BT:
				AddLoginEntity();
				break;
			case WELCOME_BQ_BT:

				mRotate3d = new Rotate3d(0, 360, v.getWidth() / 2, v.getHeight() / 2, 0, false, true);
				mRotate3d.setDuration(500);
				v.startAnimation(mRotate3d);
				break;

			case WELCOME_QXLOGIN_BT:
				Message mes1 = refreshUI.obtainMessage();
				mes1.obj = "loginGetE";
				refreshUI.sendMessage(mes1);
				if (aThread.isAlive())
				{
					aThread.stop();
				}
				break;
			case WELCOME_DL_BT:
				Intent intent=new Intent();
				intent.setClass(LoginActivity.this, DLActivity.class);
				startActivity(intent);
				break;
			default:
				break;
			}

		}
	};
	private AnimationListener mAnimationListener = new AnimationListener()
	{

		@Override
		public void onAnimationStart(Animation animation)
		{
		}

		@Override
		public void onAnimationRepeat(Animation animation)
		{
		}

		@Override
		public void onAnimationEnd(Animation animation)
		{
			if (animation == headImgAnimation)
			{
				loginPanel.setVisibility(View.VISIBLE);
				loginPaAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.l_loginbar);
				loginPanel.startAnimation(loginPaAnimation);
			}
			else if (animation == animationSet)
			{
				Message mes = refreshUI.obtainMessage();
				mes.obj = "inlogin";
				refreshUI.sendMessage(mes);

			}

		}
	};

	private void AddLoginEntity()
	{
		String uesrname = uesrnameEdit.getText().toString();
		String passward = passwardEdit.getText().toString();
		if (uesrname.length() == 0 || passward.length() == 0)
		{
			KToast.show();
			return;
		}
		else if (!Pattern.matches("[0-9a-zA-Z@.]+", uesrname))
		{
			WToast.show();
			return;
		}
		else if (uesrname.length() < 4)
		{
			AToast.show();
			return;
		}
		Map<String, Object> uMap = new HashMap<String, Object>();
		uMap.put("Uesr", uesrname);
		uMap.put("Passw", passward);
		Task aTask = new Task(Entity_Way.Login, uMap);
		NetSendThread.TaskPool.add(aTask);
		dialog1= new CustomDialog(LoginActivity.this,160,160, R.layout.layout_dialog, R.style.Theme_dialog);
		dialog1.setCancelable(false);
		dialog1.show();
	         TextView mMessage = (TextView) dialog1.findViewById(R.id.message);
	        mMessage.setText("登陆中...");
	}
	CustomDialog dialog1 ;
	public void myref(int tOrf)
	{
		if (tOrf == LoginFunction.OK)
		{

			SharedPreferences mPreferences = getSharedPreferences("first_pref", MODE_PRIVATE);
			boolean isFirstIn = mPreferences.getBoolean("isFirstIn", true);
			SToast.show();
			try
			{
				Thread.sleep(500);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Intent intent;
			if (isFirstIn)
			{
				intent = new Intent(LoginActivity.this, GuideActivity.class);
			}
			else
			{
				intent = new Intent(LoginActivity.this, MainActivity.class);
			}

			LoginActivity.this.startActivity(intent);
			LoginActivity.this.finish();
			return;
		}
		else if (tOrf == LoginFunction.NEM)
		{
			DToast.show();
		}
		else if (tOrf == LoginFunction.HAS)
		{
		}
		else if (tOrf == LoginFunction.WHAT)
		{
			WHToast.show();
		}
		else if (tOrf == LoginFunction.BUG)
		{
			CToast.show();
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler refreshUI = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			if (msg.obj.equals("loginGetE"))
			{
				mLayout.removeViewAt(mLayout.getChildCount() - 1);
			}
			else if (msg.obj.equals("loginGetE2"))
			{
				View logingView = getLayoutInflater().inflate(R.layout.tongzhibar, mLayout, true);
				logingAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.loging_out);
				Animation btmbgAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.btmbg_out);
				logingbar = (LinearLayout) logingView.findViewById(R.id.logingbar);
				View btmbg = (View) logingView.findViewById(R.id.btmbg);
				logingbar.startAnimation(logingAnimation);
				btmbg.startAnimation(btmbgAnimation);
				Button qxlogButton = (Button) logingView.findViewById(R.id.qxlog);
				qxlogButton.setTag(MY_BT.WELCOME_QXLOGIN_BT);
				qxlogButton.setOnClickListener(mClickListener);
			}
			else if (msg.obj.equals("inlogin"))
			{
				mLayout.removeViewsInLayout(1, mLayout.getChildCount() - 1);
				View loginView = getLayoutInflater().inflate(R.layout.login, mLayout, true);
				QuickContactBadge headBadge = (QuickContactBadge) loginView.findViewById(R.id.quickContactBadge1);
				loginButton = (Button) loginView.findViewById(R.id.login_bt);
				loginPanel = (LinearLayout) loginView.findViewById(R.id.loginban);
				uesrnameEdit = (EditText) findViewById(R.id.editText1);
				passwardEdit = (EditText) findViewById(R.id.editText2);
				TextView dl = (TextView) findViewById(R.id.textView2);
				dl.setTag(MY_BT.WELCOME_DL_BT);
				dl.setOnClickListener(mClickListener);
				loginPanel.setVisibility(View.INVISIBLE);

				headImgAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.pct_out);
				loginBtAnimation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.shuchu);
				headImgAnimation.setAnimationListener(mAnimationListener);
				headBadge.startAnimation(headImgAnimation);
				loginButton.startAnimation(loginBtAnimation);
				loginButton.setTag(MY_BT.WELCOME_LOGIN_BT);
				loginButton.setOnClickListener(mClickListener);
			}
		}

	};
}
