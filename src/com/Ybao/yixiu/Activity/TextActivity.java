package com.Ybao.yixiu.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.Ybao.yixiu.R;

public class TextActivity extends Activity implements OnClickListener
{
	private static String Txt = "";
	private EditText txtV;

	public static void setTxt(String txt)
	{
		Txt=txt;
	}

	@SuppressLint("CutPasteId")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text_eidt);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.title_bar);
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("��݋�б�");
		Button bcbt = (Button) findViewById(R.id.l_bt);
		bcbt.setTag("BACK_BT");
		bcbt.setText("����");
		bcbt.setOnClickListener(this);
		txtV = (EditText) findViewById(R.id.text_content);
		if (Txt!=null)
		{
			txtV.setText(Txt);
		}


	}

	@Override
	public void onClick(View v)
	{
		String tag = v.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent keyEvent = new KeyEvent(0, 4);
			onKeyDown(4, keyEvent);
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		case 4:
			Txt = txtV.getText().toString();
			Intent intent = new Intent();
			intent.putExtra("content", Txt);
			setResult(4, intent);
			finish();
			return false;
		default:
			return super.onKeyDown(keyCode, event);
		}
	}

}
