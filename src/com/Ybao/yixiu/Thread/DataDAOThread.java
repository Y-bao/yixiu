package com.Ybao.yixiu.Thread;

import java.util.ArrayList;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.Ybao.yixiu.DataBase.ConnDAO;
import com.Ybao.yixiu.Entity.Task;
import com.Ybao.yixiu.Fragment.Page1Fragment;
import com.Ybao.yixiu.Fragment.Page2Fragment;
import com.Ybao.yixiu.Function.EntityFunction;
import com.Ybao.yixiu.Ini.NetParamete;
import com.Ybao.yixiu.Pool.ActivityPool;

public class DataDAOThread extends Service implements Runnable
{
	public static boolean keepon = true;
	public static ArrayList<Task> TaskPool = new ArrayList<Task>();

	private void executive(Task task)
	{
		ConnDAO cDao = new ConnDAO(ActivityPool.getActivityByName("LoginActivity"));
		switch (task.getTaskId())
		{
		case GetApply:
			Page1Fragment.mRefreshList.List = cDao.RuturnList(task.getMap(), "t_apply");
			break;
		case GetMessage:
			Page2Fragment.mRefreshList.List = cDao.RuturnList(task.getMap(), "t_message");
			break;
		case GetUpDataTime:
			NetParamete.NewUpDataTime = cDao.RuturnNew("t_updata");
			EntityFunction.goon = true;
			break;
		case InSetUesrInfos:
			cDao.ExecuteProcedure(task.getMap(), "t_uesrInfos");
			Page1Fragment.mRefreshList.List = cDao.RuturnList(null, "t_apply");
			Page2Fragment.mRefreshList.List = cDao.RuturnList(null, "t_message");
			break;
		default:
			break;
		}
	}

	@Override
	public void run()
	{
		while (keepon)
		{
			try
			{
				Thread.sleep(200);
			}
			catch (Exception e)
			{
			}
			if (TaskPool.size() > 0)
			{
				executive(TaskPool.get(0));
				TaskPool.remove(0);
			}
		}

	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}
}
