package com.Ybao.yixiu.Function;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

import android.util.Log;

import com.Ybao.yixiu.DataBase.ConnDAO;
import com.Ybao.yixiu.Entity.Message;
import com.Ybao.yixiu.Fragment.Page1Fragment;
import com.Ybao.yixiu.Fragment.Page2Fragment;
import com.Ybao.yixiu.Ini.NetParamete;
import com.Ybao.yixiu.Ini.UesrData;
import com.Ybao.yixiu.Pool.ActivityPool;

public class EntityFunction
{
	public final static String APPLY = "apply";
	public final static String MESSAGE = "message";
	public final static String UESRINFOS = "uesrinfos";
	public final static String UPDATA = "updata";
	public final static int OK = 2;
	public final static int BUG = -1;
	public final static int WHAT = -2;
	public static InputStream in;
	public static OutputStream out;
	public static boolean goon = false;

	public static boolean cion()
	{
		try
		{
			InetAddress address = InetAddress.getByName(NetParamete.IP);
			Socket socket = new Socket(address, NetParamete.Port2);
			socket.setSoTimeout(NetParamete.Timeout);
			out = socket.getOutputStream();
			in = socket.getInputStream();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public synchronized static void Macth()
	{
		try
		{
			Log.i("a", "st");
			if (!cion())
			{
				return;
			}
			Message msg = new Message("Macth");
			msg.Parameters.put("Id", UesrData.ID);
			Log.i("a", msg.toJson());
			out.write(msg.toJson().getBytes());
			out.flush();
			Log.i("a", "ok");
		}
		catch (Exception e)
		{
		}

	}

	public synchronized static void ReceiveMassage()
	{
		try
		{
			byte[] tf = new byte[1024];
			in.read(tf);
			String json = new String(tf).trim();
			Log.i("a", json);
			Message msg = Message.getMessage(json);
			if (msg.Type.equalsIgnoreCase("DownLoadApply"))
			{
				Page1Fragment.mRefreshList.PutItemFromJson(msg.Results.get("Apply").toString(), "Applys");
			}
			else if (msg.Type.equalsIgnoreCase("DownLoadMessage"))
			{
				Page2Fragment.mRefreshList.PutItemFromJson(msg.Results.get("Message").toString(), "Messages");
			}
		}
		catch (Exception e)
		{
		}

	}

	public static String Respond(String Json) throws IOException
	{
		Message msg = Message.getMessage(Json);
		if (msg.Type.equalsIgnoreCase(APPLY))
		{

		}
		else if (msg.Type.equalsIgnoreCase(MESSAGE))
		{

		}
		return "";
	}

	public static void InDAO(String Json)
	{
		Message msg = Message.getMessage(Json);
		ConnDAO connDAO = new ConnDAO(ActivityPool.getActivityByName("LoginActivity"));
		if (msg.Type.equalsIgnoreCase(APPLY))
		{
			connDAO.ExecuteProcedure(msg.Results, "t_apply");
		}
	}

	public synchronized static void Close()
	{
		try
		{
			out.close();
			in.close();
		}
		catch (Exception e)
		{
		}
	}
}
