package com.Ybao.yixiu.Thread;

import java.util.ArrayList;

import android.R.integer;
import android.os.AsyncTask;
import android.util.Log;

import com.Ybao.yixiu.Activity.LoginActivity;
import com.Ybao.yixiu.Entity.RefreshList;
import com.Ybao.yixiu.Entity.Task;
import com.Ybao.yixiu.Fragment.Page1Fragment;
import com.Ybao.yixiu.Fragment.Page2Fragment;
import com.Ybao.yixiu.Function.LoginFunction;
import com.Ybao.yixiu.Pool.ActivityPool;

public class NetSendThread extends AsyncTask<integer, integer, String>
{

	public static boolean keepon = true;
	public static ArrayList<Task> TaskPool = new ArrayList<Task>();

	private void executive(Task aTask)
	{
		switch (aTask.getTaskId())
		{
		case Login:
			String Uesr = aTask.getMap().get("Uesr").toString();
			String Passw = aTask.getMap().get("Passw").toString();
			int n = LoginFunction.Login(Uesr, Passw);
			LoginActivity wa = (LoginActivity) ActivityPool.getActivityByName("LoginActivity");
					wa.myref(LoginFunction.OK);
			if (n == LoginFunction.OK)
			{
				Page1Fragment.mRefreshList = new RefreshList();
				Page2Fragment.mRefreshList = new RefreshList();
				LoginFunction.InitializeData();
				publishProgress();
			}
			break;
		case SendApply:
			Log.i("a", "SendApply");
			LoginFunction.sendApply(aTask.getMap());
			break;
		case Logout:
			LoginFunction.Close();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onProgressUpdate(integer... values)
	{
		Thread receiveThread = new Thread(new NetReceiveThread());
		receiveThread.start();
	}

	@Override
	protected String doInBackground(integer... arg0)
	{
		while (keepon)
		{
			try
			{
				Thread.sleep(200);
			}
			catch (Exception e)
			{
			}
			if (TaskPool.size() > 0)
			{
				executive(TaskPool.get(0));
				TaskPool.remove(0);
			}
		}
		return null;
	}

}
