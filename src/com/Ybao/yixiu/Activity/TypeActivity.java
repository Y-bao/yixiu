package com.Ybao.yixiu.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.Ybao.yixiu.R;

public class TypeActivity extends Activity implements OnClickListener
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_type_set);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.title_bar);
		}
		TextView tvtxt = (TextView) findViewById(R.id.titletext);
		tvtxt.setText("�������");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("����");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		String tag = v.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent keyEvent = new KeyEvent(0, 4);
			onKeyDown(4, keyEvent);
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		case 4:
			Intent intent = new Intent();
			intent.putExtra("type", "����");
			setResult(3, intent);
			finish();
			return false;
		default:
			return super.onKeyDown(keyCode, event);
		}
	}
}
