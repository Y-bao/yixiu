package com.Ybao.yixiu.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Activity.MessageActivity;
import com.Ybao.yixiu.Entity.RefreshList;
import com.Ybao.yixiu.Entity.RefreshList.onRefrash;

public class Page2Fragment extends Fragment implements OnItemClickListener
{
	ListView mListView;
	public static RefreshList mRefreshList = new RefreshList();
	BaseAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View mView = inflater.inflate(R.layout.page2, null);
		mListView = (ListView) mView.findViewById(R.id.message_list);
		mListView.setEmptyView(mView.findViewById(R.id.message_list_empty));
		mRefreshList.setOnRefrash(myRefrash);
		mAdapter = new SimpleAdapter(inflater.getContext(), mRefreshList.List,
				R.layout.message_item, new String[] { "Title", "Time" },
				new int[] { R.id.title, R.id.info });
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		return mView;
	}

	private onRefrash myRefrash = new onRefrash()
	{
		@Override
		public void onCanRefrash()
		{
			Message msg = myHandler.obtainMessage();
			myHandler.sendMessage(msg);
		}
	};

	Handler myHandler = new Handler()
	{

		@Override
		public void handleMessage(Message msg)
		{
			mAdapter.notifyDataSetChanged();
		}

	};

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		Intent intent = new Intent();
		intent.putExtra("index", arg2);
		intent.setClass(getActivity(), MessageActivity.class);
		startActivity(intent);
	}
}
