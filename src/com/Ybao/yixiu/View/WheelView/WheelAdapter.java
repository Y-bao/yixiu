package com.Ybao.yixiu.View.WheelView;

public interface WheelAdapter
{
	public int getItemsCount();

	public String getItem(int index);

	public int getMaximumLength();
}
