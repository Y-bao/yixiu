package com.Ybao.yixiu.View.WheelView;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import android.view.View;

import com.Ybao.yixiu.R;

public class WheelMain
{

	private View view;
	private WheelView wv_year;
	private WheelView wv_month;
	private WheelView wv_day;
	private static int START_YEAR = 1990, END_YEAR = 2100;
	private List<String> list_big;
	private List<String> list_little;

	public View getView()
	{
		return view;
	}

	public void setView(View view)
	{
		this.view = view;
	}

	public static int getSTART_YEAR()
	{
		return START_YEAR;
	}

	public static void setSTART_YEAR(int sTART_YEAR)
	{
		START_YEAR = sTART_YEAR;
	}

	public static int getEND_YEAR()
	{
		return END_YEAR;
	}

	public static void setEND_YEAR(int eND_YEAR)
	{
		END_YEAR = eND_YEAR;
	}

	public WheelMain(View view)
	{
		super();

		this.view = view;
		setView(view);
		String[] months_big = { "1", "3", "5", "7", "8", "10", "12" };
		String[] months_little = { "4", "6", "9", "11" };
		list_big = Arrays.asList(months_big);
		list_little = Arrays.asList(months_little);
	}

	public void initDateTimePicker()
	{

		wv_year = (WheelView) view.findViewById(R.id.year);
		wv_year.setAdapter(new NumericWheelAdapter(START_YEAR, END_YEAR));
		wv_year.setCyclic(true);
		wv_year.setLabel("��");

		wv_month = (WheelView) view.findViewById(R.id.month);
		wv_month.setAdapter(new NumericWheelAdapter(1, 12));
		wv_month.setCyclic(true);
		wv_month.setLabel("��");

		wv_day = (WheelView) view.findViewById(R.id.day);
		wv_day.setCyclic(true);
		wv_day.setLabel("��");

		wv_year.addChangingListener(wheelListener_year);
		wv_month.addChangingListener(wheelListener_month);
		setTime();
	}

	public void setTime()
	{
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		wv_year.setCurrentItem(year - START_YEAR);
		wv_month.setCurrentItem(month);
		wv_day.setCurrentItem(day - 1);
	}

	public void setTime(int year, int month, int day)
	{
		wv_year.setCurrentItem(year - START_YEAR);
		wv_month.setCurrentItem(month - 1);
		wv_day.setCurrentItem(day - 1);
	}

	public int[] getTime()
	{
		int[] Date = { (wv_year.getCurrentItem() + START_YEAR),
				(wv_month.getCurrentItem() + 1), (wv_day.getCurrentItem() + 1) };
		return Date;
	}

	OnWheelChangedListener wheelListener_year = new OnWheelChangedListener()
	{
		public void onChanged(WheelView wheel, int oldValue, int newValue)
		{
			int year_num = newValue + START_YEAR;

			int n;
			if (list_big
					.contains(String.valueOf(wv_month.getCurrentItem() + 1)))
			{
				n = 31;
			}
			else if (list_little.contains(String.valueOf(wv_month
					.getCurrentItem() + 1)))
			{
				n = 30;
			}
			else
			{
				if ((year_num % 4 == 0 && year_num % 100 != 0)
						|| year_num % 400 == 0)
				{
					n = 29;
				}
				else
				{
					n = 28;
				}

			}
			wv_day.setAdapter(new NumericWheelAdapter(1, n));
			if ((wv_day.getCurrentItem() + 1) > n)
			{
				wv_day.setCurrentItem(n-1);
			}
		}
	};
	OnWheelChangedListener wheelListener_month = new OnWheelChangedListener()
	{
		public void onChanged(WheelView wheel, int oldValue, int newValue)
		{
			int month_num = newValue + 1;
			int n;
			if (list_big.contains(String.valueOf(month_num)))
			{
				n = 31;
			}
			else if (list_little.contains(String.valueOf(month_num)))
			{
				n = 30;
			}
			else
			{
				if (((wv_year.getCurrentItem() + START_YEAR) % 4 == 0 && (wv_year
						.getCurrentItem() + START_YEAR) % 100 != 0)
						|| (wv_year.getCurrentItem() + START_YEAR) % 400 == 0)
				{
					n = 29;
				}
				else
				{
					n = 28;
				}
			}
			wv_day.setAdapter(new NumericWheelAdapter(1, n));
			if ((wv_day.getCurrentItem() + 1) > n)
			{
				wv_day.setCurrentItem(n-1);
			}
		}
	};
}
