package com.Ybao.yixiu.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;

public class ControllableSlideGroup extends SimpleSlideGroup
{
	private boolean canshowLeft = true;
	private boolean canshowRight = false;
	private boolean tCanSlideLeft = true;
	private boolean tCanSlideRight = false;
	private boolean hasClickLeft = false;
	private boolean hasClickRight = false;

	// private boolean fanwei = false;

	public ControllableSlideGroup(Context context)
	{
		super(context);
	}

	public ControllableSlideGroup(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public void setCanSliding(boolean left, boolean right)
	{
		if (leftView != null)
		{
			canshowLeft = left;
		}

		if (rightView != null)
		{
			canshowRight = right;
		}
	}

	// 这个感觉没什么作用 不管true还是false 都是会执行onTouchEvent的 因为子view里面onTouchEvent返回false了
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		final int action = ev.getAction();
		final float x = ev.getX();
		float oldScrollX = centerGroup.getScrollX();

		switch (action)
		{
		case MotionEvent.ACTION_DOWN:
			// fanwei = x < -oldScrollX || x > centerGroup.getWidth() -
			// oldScrollX ? false : true;
			if (!mScroller.isFinished())
			{
				mIsBeingDragged = true;
				mScroller.abortAnimation();
			}
			mLastMotionX = x;
			break;
		case MotionEvent.ACTION_MOVE:
			// if (x < -oldScrollX || x > centerGroup.getWidth() - oldScrollX)
			// {
			// mIsBeingDragged = false;
			// if (fanwei)
			// {
			// return true;
			// }
			// else
			// {
			// return false;
			// }
			// }
			final float xDiff = mLastMotionX - x;

			if ((canshowLeft && (xDiff < 0 || oldScrollX < 0)) || (canshowRight && (xDiff > 0 || oldScrollX > 0)))
			{
				if (Math.abs(xDiff) > mTouchSlop)
				{
					mIsBeingDragged = true;
					mLastMotionX = x;
				}
			}
			break;
		}
		return mIsBeingDragged /* && fanwei */;
	}

	public boolean onTouchEvent(MotionEvent event)
	{

		if (mIsBeingDragged)
		{
			if (mVelocityTracker == null)
			{
				mVelocityTracker = VelocityTracker.obtain();
			}
			mVelocityTracker.addMovement(event);
			float x = event.getX();
			float oldScrollX = centerGroup.getScrollX();

			switch (event.getAction())
			{
			case MotionEvent.ACTION_MOVE:
				final float deltaX = mLastMotionX - x;
				mLastMotionX = x;
				float scrollX = oldScrollX + deltaX;
				if (scrollX > 0)
				{
					if (canshowLeft)
					{
						scrollX = 0;
					}
				}
				else if (scrollX < 0)
				{
					if (canshowRight)
					{
						scrollX = 0;
					}
				}
				if (deltaX < 0 && oldScrollX < 0)
				{
					final float leftBound = 0;
					final float rightBound = -getLeftWidth();
					if (scrollX > leftBound)
					{
						scrollX = leftBound;
					}
					else if (scrollX < rightBound)
					{
						scrollX = rightBound;
						// scrollX = (int) (oldScrollX + deltaX / 2);
					}
					showLeft();
				}
				else if (deltaX > 0 && oldScrollX > 0)
				{
					final float rightBound = getRightWidth();
					final float leftBound = 0;
					if (scrollX < leftBound)
					{
						scrollX = leftBound;
					}
					else if (scrollX > rightBound)
					{
						scrollX = rightBound;
						// scrollX = (int) (oldScrollX + deltaX / 2);
					}
					showRight();
				}
				if (centerGroup != null)
				{
					centerGroup.scrollTo((int) scrollX, centerGroup.getScrollY());
				}

				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				final VelocityTracker velocityTracker = mVelocityTracker;
				velocityTracker.computeCurrentVelocity(1000);
				float xVelocity = velocityTracker.getXVelocity();
				int dx = 0;
				if (oldScrollX <= 0 && canshowLeft)
				{
					if (xVelocity > SNAP_VELOCITY)
					{
						showLeft();
						dx = (int) (-getLeftWidth() - oldScrollX);
					}
					else if (xVelocity < -SNAP_VELOCITY)
					{
						dx = (int) -oldScrollX;
						if (hasClickLeft)
						{
							hasClickLeft = false;
							setCanSliding(tCanSlideLeft, tCanSlideRight);
						}
					}
					else if (oldScrollX < -getLeftWidth() / 2)
					{

						showLeft();
						dx = (int) (-getLeftWidth() - oldScrollX);
					}
					else if (oldScrollX >= -getLeftWidth() / 2)
					{

						dx = (int) -oldScrollX;
						if (hasClickLeft)
						{
							hasClickLeft = false;
							setCanSliding(tCanSlideLeft, tCanSlideRight);
						}
					}
				}
				if (oldScrollX >= 0 && canshowRight)
				{
					if (xVelocity < -SNAP_VELOCITY)
					{
						showRight();
						dx = (int) (getRightWidth() - oldScrollX);
					}
					else if (xVelocity > SNAP_VELOCITY)
					{
						dx = (int) -oldScrollX;
						if (hasClickRight)
						{
							hasClickRight = false;
							setCanSliding(tCanSlideLeft, tCanSlideRight);
						}
					}
					else if (oldScrollX > getRightWidth() / 2)
					{
						showRight();
						dx = (int) (getRightWidth() - oldScrollX);
					}
					else if (oldScrollX <= getRightWidth() / 2)
					{
						dx = (int) -oldScrollX;
						if (hasClickRight)
						{
							hasClickRight = false;
							setCanSliding(tCanSlideLeft, tCanSlideRight);
						}
					}
				}
				smoothScrollToByTime(dx);
				mIsBeingDragged = false;
				mVelocityTracker.recycle();
				break;
			}
		}
		return true;
	}

	@Override
	public void showLeftView()
	{
		int oldScrollX = centerGroup.getScrollX();
		if (oldScrollX == 0)
		{
			showLeft();
			smoothScrollToByTime(-getLeftWidth());
			tCanSlideLeft = canshowLeft;
			tCanSlideRight = canshowRight;
			hasClickLeft = true;
			setCanSliding(true, false);
		}
		else if (oldScrollX == -getLeftWidth())
		{
			smoothScrollToByTime(getLeftWidth());
			if (hasClickLeft)
			{
				hasClickLeft = false;
				setCanSliding(tCanSlideLeft, tCanSlideRight);
			}
		}
	}

	@Override
	public void showRightView()
	{
		int oldScrollX = centerGroup.getScrollX();
		if (oldScrollX == 0)
		{
			showRight();
			smoothScrollToByTime(getRightWidth());
			tCanSlideLeft = canshowLeft;
			tCanSlideRight = canshowRight;
			hasClickRight = true;
			setCanSliding(false, true);
		}
		else if (oldScrollX == getRightWidth())
		{
			smoothScrollToByTime(-getRightWidth());
			if (hasClickRight)
			{
				hasClickRight = false;
				setCanSliding(tCanSlideLeft, tCanSlideRight);
			}
		}
	}
}
