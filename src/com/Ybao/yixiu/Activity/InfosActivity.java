package com.Ybao.yixiu.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Ini.UesrData;
import com.Ybao.yixiu.View.IconPreference;

public class InfosActivity extends PreferenceActivity implements OnClickListener
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{

		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.infos_list);
		setContentView(R.layout.activity_uesrinfos);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("��݋�б�");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("����");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
		IconPreference p1 = (IconPreference) findPreference("key_view_head");
		p1.setIcon(R.drawable.aio_face_default);
		Preference p = findPreference("key_txt_nc");
		p.setSummary(UesrData.name);
		p = findPreference("key_txt_ID");
		p.setSummary(Integer.toString(UesrData.ID));
		p1 = (IconPreference) findPreference("key_view_qr");
		p1.setIcon(R.drawable.qr);
		p = findPreference("key_txt_name");
		p.setSummary(UesrData.name);
		p = findPreference("key_txt_old");
		p.setSummary(UesrData.age);
		p = findPreference("key_txt_sex");
		p.setSummary(UesrData.sex);
		p = findPreference("key_txt_phone");
		p.setSummary(UesrData.phone);
		p = findPreference("key_txt_pos");
		p.setSummary(UesrData.position);

	}

	@Override
	public void onClick(View arg0)
	{
		if (arg0.getTag().equals("BACK_BT"))
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			this.onKeyDown(4, myKey);
		}

	}

	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference)
	{
		String str = preference.getKey();
		if ("key_view_qr".equals(str))
		{
			Intent intent = new Intent();
			intent.setClass(this, IDCodeActivity.class);
			startActivity(intent);
		}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

}
