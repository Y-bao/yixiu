package com.Ybao.yixiu.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Entity.ListItem;
import com.Ybao.yixiu.Way.MyAdapter.mtleItemList;

public class DetailActivity extends Activity implements OnClickListener
{
	private ListView listView; // 声明列表视图对象
	TextView mtextView;
	mtleItemList adapter;
	EditText detailEdit;
	mtleItemList mAdapter_ListGroup;
	List<ListItem> list_GroupItem;
	ImageButton sendButton;
	public static DetailActivity ma;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_detail);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("好友列表");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("返回");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
		ma = this;
		listView = (ListView) findViewById(R.id.detail_list);
		list_GroupItem = new ArrayList<ListItem>();
		mAdapter_ListGroup = new mtleItemList(this, list_GroupItem);
		mAdapter_ListGroup.AddType(R.layout.item1);
		mAdapter_ListGroup.AddType(R.layout.item2);
		listView.setAdapter(mAdapter_ListGroup);

		list_GroupItem.add(new ListItem(0, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		list_GroupItem.add(new ListItem(1, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		list_GroupItem.add(new ListItem(0, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));

		list_GroupItem.add(new ListItem(1, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		list_GroupItem.add(new ListItem(0, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		list_GroupItem.add(new ListItem(1, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));

		list_GroupItem.add(new ListItem(0, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		list_GroupItem.add(new ListItem(1, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		list_GroupItem.add(new ListItem(0, setHashMap(R.drawable.th,
				"testtesttesttesttesttesttesttesttesttesttesttesttesttest")));
		mAdapter_ListGroup.notifyDataSetChanged();
		sendButton = (ImageButton) findViewById(R.id.send_bt);
		detailEdit = (EditText) findViewById(R.id.detail_edit);
		detailEdit.addTextChangedListener(textWatcher);
		sendButton.setTag("send_bt");
		sendButton.setOnClickListener(this);
		sendButton.setEnabled(false);
	}

	@SuppressLint("UseSparseArrays")
	private HashMap<Integer, Object> setHashMap(Object s1, String s2)
	{
		HashMap<Integer, Object> map1 = new HashMap<Integer, Object>();
		map1.put(R.id.bigtv, s2);
		map1.put(R.id.iv, s1);
		return map1;
	}
	Timer timer;
	@Override
	public void onClick(View arg0)
	{
		String tag = arg0.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			this.onKeyDown(4, myKey);
		}
		else if (tag.equalsIgnoreCase("send_bt"))
		{
			final Handler handler = new Handler()
			{
				public void handleMessage(Message msg)
				{
					switch (msg.what)
					{
					case 1:
						timer.cancel();
						list_GroupItem.add(new ListItem(0, setHashMap(R.drawable.th, detailtxt)));
						mAdapter_ListGroup.notifyDataSetChanged();
						listView.setSelection(listView.getBottom());
						break;
					}
					super.handleMessage(msg);
				}
			};
			TimerTask task = new TimerTask()
			{
				public void run()
				{
					Message message = new Message();
					message.what = 1;
					handler.sendMessage(message);
				}
			};
			detailtxt = detailEdit.getText().toString();
			detailEdit.setText("");
			if (detailtxt != null && !"".equalsIgnoreCase(detailtxt))
			{
				list_GroupItem.add(new ListItem(1, setHashMap(R.drawable.th, detailtxt)));
				mAdapter_ListGroup.notifyDataSetChanged();
				listView.setSelection(listView.getBottom());

				timer = new Timer(true);
				timer.schedule(task,2000, 2000);
			}
		}

	}

	String detailtxt;

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}

	TextWatcher textWatcher = new TextWatcher()
	{

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after)
		{
		}

		@Override
		public void afterTextChanged(Editable s)
		{
			if (detailEdit.getText() != null && !detailEdit.getText().toString().equalsIgnoreCase(""))
			{
				sendButton.setEnabled(true);
			}
			else
			{
				sendButton.setEnabled(false);
			}
		}
	};

}
