package com.Ybao.yixiu.View.WheelView;

public interface OnWheelScrollListener
{
	void onScrollingStarted(WheelView wheel);

	void onScrollingFinished(WheelView wheel);
}
