package com.Ybao.yixiu.Fragment;

import java.util.HashMap;
import java.util.Map;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Activity.ApplyActivity;
import com.Ybao.yixiu.Entity.RefreshList;
import com.Ybao.yixiu.Entity.RefreshList.onRefrash;

public class Page1Fragment extends Fragment implements OnItemClickListener
{
	ListView mListView;
	public static RefreshList mRefreshList = new RefreshList();
	static{for(int i=0;i<20;i++){
		Map<String, Object> arg0=new HashMap<String, Object>();
		arg0.put("Title", "123");
		arg0.put("Time", "123");
		arg0.put("State", "123");
		mRefreshList.List.add(arg0);
	}};
	BaseAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View mView = inflater.inflate(R.layout.page1, null);
		mListView = (ListView) mView.findViewById(R.id.apply_list);
		mListView.setEmptyView(mView.findViewById(R.id.apply_list_empty));
		mRefreshList.setOnRefrash(myRefrash);
		mAdapter = new SimpleAdapter(inflater.getContext(), mRefreshList.List, R.layout.apply_item, new String[] {
				"Title", "Time", "State" }, new int[] { R.id.title, R.id.info, R.id.img });
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
		return mView;
	}

	private onRefrash myRefrash = new onRefrash()
	{
		@Override
		public void onCanRefrash()
		{
			Message msg = myHandler.obtainMessage();
			myHandler.sendMessage(msg);
		}
	};

	Handler myHandler = new Handler()
	{

		@Override
		public void handleMessage(Message msg)
		{
			mAdapter.notifyDataSetChanged();
		}

	};

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		Intent intent = new Intent();
		intent.putExtra("index", arg2);
		intent.setClass(getActivity(), ApplyActivity.class);
		startActivity(intent);
	}
}
