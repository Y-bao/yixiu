package com.Ybao.yixiu.Activity;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.Ybao.yixiu.R;
import com.Ybao.yixiu.Dialog.PaixuDialog;
import com.Ybao.yixiu.Entity.Task;
import com.Ybao.yixiu.Fragment.Page1Fragment;
import com.Ybao.yixiu.Ini.MyEmun.Entity_Way;
import com.Ybao.yixiu.Pool.ActivityPool;
import com.Ybao.yixiu.Thread.NetSendThread;

public class EidtActivity extends PreferenceActivity implements OnClickListener
{

	private String[] Summary;
	private String[] tags = { "Title", "Time", "Time", "Type", "Text" };
	private Toast toast;
	private Toast toast1;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		boolean titlebar = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.apply_eidt);
		setContentView(R.layout.activity_apply_eidt);
		if (titlebar)
		{
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
		}
		TextView tvTitle = (TextView) findViewById(R.id.titletext);
		tvTitle.setText("編輯列表");
		Button tvbt = (Button) findViewById(R.id.l_bt);
		tvbt.setText("返回");
		tvbt.setTag("BACK_BT");
		tvbt.setOnClickListener(this);
		Button tjbt = (Button) findViewById(R.id.send_apply);
		tjbt.setTag("SEND_BT");
		tjbt.setOnClickListener(this);

		System.out.println("0");
		Summary = new String[5];
		toast = Toast.makeText(EidtActivity.this, "请补全信息后提交！！", Toast.LENGTH_SHORT);
		toast1 = Toast.makeText(EidtActivity.this, "已将修改保持到草稿箱", Toast.LENGTH_SHORT);
	}

	@Override
	public void onClick(View arg0)
	{
		String tag = arg0.getTag().toString();
		if (tag.equalsIgnoreCase("BACK_BT"))
		{
			KeyEvent myKey = new KeyEvent(0, 4);
			this.onKeyDown(4, myKey);
		}
		else if (tag.equalsIgnoreCase("SEND_BT"))
		{
			Map<String, Object> map = new HashMap<String, Object>();
			for (int i = 0; i < Summary.length; i++)
			{
				if (Summary[i] == null || Summary[i].length() == 0)
				{
					toast.show();
					return;
				}
				if (i == 1)
				{
					String s = Summary[i] + "~" + Summary[i + 1];
					map.put(EidtActivity.this.tags[i], s);
				}
				else if (i != 2)
				{
					map.put(EidtActivity.this.tags[i], Summary[i]);
				}
			}
			map.put("StateID", 1);
			Task aTask = new Task(Entity_Way.SendApply, map);
			NetSendThread.TaskPool.add(aTask);
			map.put("State", R.drawable.apply_ing);
			Page1Fragment.mRefreshList.addItem(map, 1);
			Page1Fragment.mRefreshList.addItem(null, 0);
			MainActivity ma = (MainActivity) ActivityPool.getActivityByName("MainActivity");
			Intent intent = new Intent();
			intent.putExtra("index", Page1Fragment.mRefreshList.List.size()-1);
			intent.setClass(EidtActivity.this, ApplyActivity.class);
			startActivity(intent);
			//ma.mLayout.showDetailViewR();
			ma.mFragment.topage(0);
			EidtActivity.this.finish();
			overridePendingTransition(R.anim.da,R.anim.fei); 
		}
	}

	@Override
	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
	{
		switch (paramInt)
		{
		case 4:
			for (int i = 0; i < Summary.length; i++)
			{
				if (Summary[i] != null)
				{
					PaixuDialog Dialog = new PaixuDialog(EidtActivity.this, 250, 220, PaixuDialog.WICON, "询问",
							"是否保持修改到草稿箱？");
					Dialog.setCanceledOnTouchOutside(false);
					Dialog.SetdonoButton(new OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							TimeActivity.setStartTime(null);
							TimeActivity.setEndTime(null);
							TextActivity.setTxt(null);
							EidtActivity.this.finish();
						}
					});
					Dialog.SetdoyesButton(new OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							Map<String, Object> map = new HashMap<String, Object>();
							map.put(tags[0], Summary[0]);
							map.put(tags[1], Summary[1] + "~" + Summary[2]);
							map.put(tags[3], Summary[3]);
							map.put(tags[4], Summary[4]);
							DraftActivity.mRefreshList.addItem(map, 1);
							TimeActivity.setStartTime(null);
							TimeActivity.setEndTime(null);
							TextActivity.setTxt(null);
							EidtActivity.this.toast1.show();
							Intent intent = new Intent(EidtActivity.this, DraftActivity.class);
							startActivity(intent);
							EidtActivity.this.finish();
						}
					});
					Dialog.show();
					return false;
				}
			}
			this.finish();
			return false;
		default:
			return super.onKeyDown(paramInt, paramKeyEvent);
		}
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference)
	{
		String str = preference.getKey();
		if ("key_eidt_name".equals(str))
		{
			Intent intent = new Intent();
			intent.setClass(this, NameActivity.class);
			startActivityForResult(intent, 1);
		}
		else if ("key_time_start".equals(str))
		{
			Intent intent = new Intent();
			intent.setClass(this, TimeActivity.class);
			intent.putExtra("time_type", 1);
			startActivityForResult(intent, 2);
		}
		else if ("key_time_end".equals(str))
		{
			Intent intent = new Intent();
			intent.setClass(this, TimeActivity.class);
			intent.putExtra("time_type", 2);
			startActivityForResult(intent, 3);
		}

		else if ("key_type_set".equals(str))
		{
			Intent intent = new Intent();
			intent.setClass(this, TypeActivity.class);
			startActivityForResult(intent, 4);
		}
		else if ("key_txt_set".equals(str))
		{
			Intent intent = new Intent();
			intent.setClass(this, TextActivity.class);
			startActivityForResult(intent, 5);
		}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == 1)
		{
			Bundle dBundle = data.getExtras();
			String str = dBundle.getString("eidt_name");
			Preference p = findPreference("key_eidt_name");
			if (str.length() != 0)
			{
				p.setSummary(str);
				Summary[0] = str;
			}
		}
		else if (resultCode == 2)
		{
			Preference p1 = findPreference("key_time_start");
			Preference p2 = findPreference("key_time_end");
			int[] it1 = TimeActivity.getStartTime();
			int[] it2 = TimeActivity.getEndTime();
			if (it1 != null)
			{
				StringBuffer sb = new StringBuffer();
				sb.append(it1[0]).append(",").append(it1[1]).append(",").append(it1[2]);
				p1.setSummary(sb.toString());
				Summary[1] = sb.toString();
			}
			else
			{
				p1.setSummary("");
				Summary[1] = "";
			}
			if (it2 != null)
			{
				StringBuffer sb = new StringBuffer();
				sb.append(it2[0]).append(",").append(it2[1]).append(",").append(it2[2]);
				p2.setSummary(sb.toString());
				Summary[2] = sb.toString();
			}
			else
			{
				p2.setSummary("");
				Summary[2] = "";
			}
		}
		else if (resultCode == 3)
		{
			Bundle dBundle = data.getExtras();
			String str = dBundle.getString("type");
			Preference p = findPreference("key_type_set");
			if (str != null)
			{
				p.setSummary(str);
				Summary[3] = str;
			}
		}
		else if (resultCode == 4)
		{
			Bundle dBundle = data.getExtras();
			String str = dBundle.getString("content");
			Preference p = findPreference("key_txt_set");
			if (str != null)
			{
				p.setSummary(str);
				Summary[4] = str;
			}
		}
	}
}
