package com.Ybao.yixiu.Thread;

import java.util.ArrayList;

import android.R.integer;
import android.os.AsyncTask;
import android.util.Log;

import com.Ybao.yixiu.Activity.LoginActivity;
import com.Ybao.yixiu.Entity.Task;
import com.Ybao.yixiu.Function.LoginFunction;
import com.Ybao.yixiu.Pool.ActivityPool;

public class NetSendDetailThread extends AsyncTask<integer, integer, String>
{

	public static boolean keepon = true;
	public static ArrayList<Task> TaskPool = new ArrayList<Task>();

	private void executive(Task aTask)
	{
			LoginFunction.sendApply(aTask.getMap());
	}

	@Override
	protected void onProgressUpdate(integer... values)
	{
		Thread receiveThread = new Thread(new NetReceiveThread());
		receiveThread.start();
	}

	@Override
	protected String doInBackground(integer... arg0)
	{
		while (keepon)
		{
			try
			{
				Thread.sleep(200);
			}
			catch (Exception e)
			{
			}
			if (TaskPool.size() > 0)
			{
				executive(TaskPool.get(0));
				TaskPool.remove(0);
			}
		}
		return null;
	}

}
