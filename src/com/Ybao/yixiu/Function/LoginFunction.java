package com.Ybao.yixiu.Function;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Map;

import android.util.Log;

import com.Ybao.yixiu.Entity.Message;
import com.Ybao.yixiu.Fragment.Page1Fragment;
import com.Ybao.yixiu.Fragment.Page2Fragment;
import com.Ybao.yixiu.Ini.NetParamete;
import com.Ybao.yixiu.Ini.UesrData;
import com.google.gson.Gson;

public class LoginFunction
{
	public final static String APPLY = "apply";
	public final static String MESSAGE = "message";
	public final static String UESRINFOS = "uesrinfos";
	public final static String LOGIN = "login";
	public final static String LOGOUT = "logout";
	public final static int OK = 2;
	public final static int HAS = 1;
	public final static int NEM = 0;
	public final static int BUG = -1;
	public final static int WHAT = -2;
	public static InputStream in;
	public static OutputStream out;
	public static boolean goon = false;

	protected static boolean cion()
	{
		try
		{
			InetAddress address = InetAddress.getByName(NetParamete.IP);
			Socket socket = new Socket(address, NetParamete.Port1);
			socket.setSoTimeout(NetParamete.Timeout);
			out = socket.getOutputStream();
			in = socket.getInputStream();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public synchronized static int Login(String uesrname, String password)
	{
		try
		{
			if (!cion())
			{
				return BUG;
			}
			Message msg = new Message("Login");
			msg.Parameters.put("Id", uesrname);
			msg.Parameters.put("Password", password);
			out.write(msg.toJson().getBytes());
			out.flush();
			byte[] inReadB = new byte[1024];
			in.read(inReadB);
			String json = new String(inReadB).trim();
			Log.i("a", json);
			msg = Message.getMessage(json);
			Object obj = msg.Results.get("LoginState");
			if (obj.equals("OK"))
			{
				Log.i("a", "ll");
				UesrData.setUesr(msg.Results);
				return OK;
			}
			in.close();
			out.close();
			if (obj.equals("NEM"))
			{
				return NEM;
			}
			else if (obj.equals("HAS"))
			{
				return HAS;
			}
			else
			{
				return WHAT;
			}
		}
		catch (Exception e)
		{
			return WHAT;
		}

	}

	public synchronized static void InitializeData()
	{
		try
		{
			Message msg = new Message("Initialize");
			msg.Parameters.put("Id", UesrData.ID);
			msg.Parameters.put("UpTime", "");
			out.write(msg.toJson().getBytes());
			out.flush();
			byte[] inReadB = new byte[1540];
			in.read(inReadB);
			String json = new String(inReadB).trim();
			Log.i("a", json);
			msg = Message.getMessage(json);
			Page1Fragment.mRefreshList.AddItemFromJson(msg.Results.get("Applys").toString(), "Applys");
			Page2Fragment.mRefreshList.AddItemFromJson(msg.Results.get("Messages").toString(), "Messages");
		}
		catch (Exception e)
		{
		}
	}

	public synchronized static void sendApply(Map<String, Object> map)
	{
		try
		{
			Message msg = new Message("SendApply");
			msg.Parameters.put("Id", UesrData.ID);
			Gson gson = new Gson();
			msg.Parameters.put("Apply", gson.toJson(map));
			Log.i("a",msg.toJson());
			out.write(msg.toJson().getBytes());
			out.flush();
			byte[] inReadB = new byte[1024];
			in.read(inReadB);
			Log.i("a","ok");
		}
		catch (Exception e)
		{
		}
	}

	public synchronized static void Close()
	{
		try
		{
			Message msg = new Message("Logout");
			msg.Parameters.put("Id", UesrData.ID);
			out.write(msg.toJson().getBytes());
			out.flush();
			byte[] inReadB = new byte[50];
			in.read(inReadB);
			in.close();
			out.close();
		}
		catch (Exception e)
		{
		}
	}

}
