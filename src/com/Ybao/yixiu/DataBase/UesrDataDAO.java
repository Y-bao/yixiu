package com.Ybao.yixiu.DataBase;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.Ybao.yixiu.Ini.UesrData;

public class UesrDataDAO
{

	private DBOpenHelper helper;
	private SQLiteDatabase db;

	public UesrDataDAO(Context context)
	{
		helper = new DBOpenHelper(context);
		db = helper.getReadableDatabase();
	}

	public void addInfos(Map<String, Object> map)
	{
		db.execSQL("insert into t_uesrInfos (uesrID,Nmae,Old,Sex,phone) values (?,?,?,?,?);",
				new Object[] { map.get("uesrID"), map.get("Nmae"), map.get("Old"), map.get("Sex"), map.get("phone") });
	}

	public void getInfos()
	{
		Cursor cursor = db.rawQuery("select uesrID,Nmae,Old,Sex,phone from t_uesrInfos where uesrID=?;",
				new String[] { String.valueOf(UesrData.ID) });
		Map<String, Object> map = null;
		while (cursor.moveToNext())
		{
			map = new HashMap<String, Object>();
			map.put("uesrID", cursor.getString(0));
			map.put("Nmae", cursor.getString(1));
			map.put("Old", cursor.getString(2));
			map.put("Sex", cursor.getString(3));
			map.put("phone", cursor.getString(4));
		}
	}

	public void addUesr(Map<String, Object> map)
	{
		db.execSQL("insert into t_uesrs (uesrName,password,state) values (?,?,?);", new Object[] { map.get("uesrName"),
				map.get("password"), 1 });
	}

	public void getUesr()
	{
		Cursor cursor = db.rawQuery("select uesrName,password from t_uesrs where state=1;", null);
		Map<String, Object> map = null;
		while (cursor.moveToNext())
		{
			map = new HashMap<String, Object>();
			map.put("uesrName", cursor.getString(0));
			map.put("password", cursor.getString(1));
		}
	}

	public int ReturnUpdataFirstID()
	{
		int FirstID = 0;
		db.execSQL("select max(ID) from dbo.Updatas where Uesrs_ID=?", new Object[] {});
		return FirstID;
	}

	public void close()
	{
		db.close();
	}
}
